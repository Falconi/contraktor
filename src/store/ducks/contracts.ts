import { createActions, createReducer } from 'reduxsauce'

import { InterfaceContractsInitalState } from '../../prototypes'

/**
 * Action types & creators
 */
export const { Types, Creators } = createActions({
  contractById: ['id'],
  contractsList: ['page', 'limit'],
  contractsListSuccess: ['data', 'page', 'limit', 'count'],
  contractsListError: ['data'],
  contractByCode: ['code'],
  contractByCodeSuccess: ['data'],
  contractByCodeError: ['data'],
  clientsOptions: [],
  clientsOptionsSuccess: ['data'],
  clientsOptionsError: ['data'],
  contractSave: ['contract', 'method'],
  contractSaveSuccess: ['data', 'method', 'message'],
  contractSaveError: ['data', 'method'],
  contractDelete: ['contract'],
  contractDeleteSuccess: ['data'],
  contractDeleteError: ['data'],
  contractsListReset: [],
  contractSaveReset: [],
  contractDeleteReset: [],
  clientsOptionsReset: [],
  contractByCodeReset: [],
})

/**
 * Handlers
 */

const INITIAL_STATE: InterfaceContractsInitalState = {
  listPagination: {
    currentPage: 1,
    limit: 10,
    count: 0,
  },
  list: {
    error: false,
    message: '',
    data: '',
  },
  save: {
    error: false,
    message: '',
    data: '',
    method: '',
  },
  delete: {
    error: false,
    message: '',
  },
  clientsOptions: {
    error: false,
    message: '',
    data: '',
  },
  details: {
    error: false,
    message: '',
    data: '',
  },
}

const contractById = (state = INITIAL_STATE) => ({ ...state })

const contractsList = (state = INITIAL_STATE) => ({ ...state })

const contractsListSuccess = (
  state = INITIAL_STATE,
  action: { page: number; limit: number; count: number; data: any }
) => ({
  ...state,
  listPagination: {
    currentPage: action.page,
    limit: action.limit,
    count: action.count,
  },
  list: {
    error: false,
    message: 'Success',
    data: action.data,
  },
})

const contractsListError = (
  state = INITIAL_STATE,
  action: { data: string }
) => ({
  ...state,
  listPagination: INITIAL_STATE.listPagination,
  list: {
    error: true,
    message: action.data,
    data: [],
  },
})

const contractByCode = (state = INITIAL_STATE) => ({ ...state })

const contractByCodeSuccess = (
  state = INITIAL_STATE,
  action: { data: any }
) => ({
  ...state,
  details: {
    error: false,
    message: 'Success',
    data: action.data,
  },
})

const contractByCodeError = (
  state = INITIAL_STATE,
  action: { data: string }
) => ({
  ...state,
  details: {
    error: true,
    message: action.data,
    data: {},
  },
})

const clientsOptions = (state = INITIAL_STATE) => ({ ...state })

const clientsOptionsSuccess = (
  state = INITIAL_STATE,
  action: { page: number; limit: number; count: number; data: any }
) => ({
  ...state,
  clientsOptions: {
    error: false,
    message: 'Success',
    data: action.data,
  },
})

const clientsOptionsError = (
  state = INITIAL_STATE,
  action: { data: string }
) => ({
  ...state,
  clientsOptions: {
    error: true,
    message: action.data,
    data: [],
  },
})

const contractSave = (state = INITIAL_STATE) => ({ ...state })

const contractSaveSuccess = (
  state = INITIAL_STATE,
  action: { message: string; data: any; method: string }
) => ({
  ...state,
  save: {
    error: false,
    message: action.message,
    data: action.data,
    method: action.method,
  },
})

const contractSaveError = (
  state = INITIAL_STATE,
  action: { data: string; method: string }
) => ({
  ...state,
  save: {
    error: true,
    message: action.data,
    data: '',
    method: action.method,
  },
})

const contractDelete = (state = INITIAL_STATE) => ({ ...state })

const contractDeleteSuccess = (
  state = INITIAL_STATE,
  action: { data: string }
) => ({
  ...state,
  delete: {
    error: false,
    message: action.data,
  },
})

const contractDeleteError = (
  state = INITIAL_STATE,
  action: { data: string }
) => ({
  ...state,
  delete: {
    error: true,
    message: action.data,
  },
})

const contractSaveReset = (state = INITIAL_STATE) => {
  return {
    ...state,
    save: INITIAL_STATE.save,
  }
}

const contractDeleteReset = (state = INITIAL_STATE) => {
  return {
    ...state,
    delete: INITIAL_STATE.delete,
  }
}

const contractsListReset = (state = INITIAL_STATE) => {
  return {
    ...state,
    list: INITIAL_STATE.list,
    listPagination: INITIAL_STATE.listPagination,
  }
}

const clientsOptionsReset = (state = INITIAL_STATE) => {
  return {
    ...state,
    clientsOptions: INITIAL_STATE.clientsOptions,
  }
}

const contractByCodeReset = (state = INITIAL_STATE) => {
  return {
    ...state,
    details: INITIAL_STATE.details,
  }
}

/**
 * Reducer
 */
export default createReducer(INITIAL_STATE, {
  [Types.CONTRACT_BY_ID]: contractById,
  [Types.CONTRACTS_LIST]: contractsList,
  [Types.CONTRACTS_LIST_SUCCESS]: contractsListSuccess,
  [Types.CONTRACTS_LIST_ERROR]: contractsListError,
  [Types.CONTRACT_BY_CODE]: contractByCode,
  [Types.CONTRACT_BY_CODE_SUCCESS]: contractByCodeSuccess,
  [Types.CONTRACT_BY_CODE_ERROR]: contractByCodeError,
  [Types.CLIENTS_OPTIONS]: clientsOptions,
  [Types.CLIENTS_OPTIONS_SUCCESS]: clientsOptionsSuccess,
  [Types.CLIENTS_OPTIONS_ERROR]: clientsOptionsError,
  [Types.CONTRACT_SAVE]: contractSave,
  [Types.CONTRACT_SAVE_SUCCESS]: contractSaveSuccess,
  [Types.CONTRACT_SAVE_ERROR]: contractSaveError,
  [Types.CONTRACT_DELETE]: contractDelete,
  [Types.CONTRACT_DELETE_SUCCESS]: contractDeleteSuccess,
  [Types.CONTRACT_DELETE_ERROR]: contractDeleteError,
  [Types.CONTRACTS_LIST_RESET]: contractsListReset,
  [Types.CONTRACT_SAVE_RESET]: contractSaveReset,
  [Types.CONTRACT_DELETE_RESET]: contractDeleteReset,
  [Types.CLIENTS_OPTIONS_RESET]: clientsOptionsReset,
  [Types.CONTRACT_BY_CODE_RESET]: contractByCodeReset,
})
