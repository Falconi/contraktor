import { combineReducers } from 'redux'

import Contracts from './contracts'
import Clients from './clients'

export default combineReducers({
  Contracts,
  Clients,
})
