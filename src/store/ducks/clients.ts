import { createActions, createReducer } from 'reduxsauce'

import { InterfaceClientsInitalState } from '../../prototypes'

/**
 * Action types & creators
 */
export const { Types, Creators } = createActions({
  clientById: ['id'],
  clientsList: ['page', 'limit'],
  clientsListSuccess: ['data', 'page', 'limit', 'count'],
  clientsListError: ['data'],
  clientSave: ['client', 'method'],
  clientSaveSuccess: ['data', 'method', 'message'],
  clientSaveError: ['data', 'method'],
  clientDelete: ['client'],
  clientDeleteSuccess: ['data'],
  clientDeleteError: ['data'],
  clientsListReset: [],
  clientSaveReset: [],
  clientDeleteReset: [],
})

/**
 * Handlers
 */

const INITIAL_STATE: InterfaceClientsInitalState = {
  listPagination: {
    currentPage: 1,
    limit: 10,
    count: 0,
  },
  list: {
    error: false,
    message: '',
    data: '',
  },
  save: {
    error: false,
    message: '',
    data: '',
    method: '',
  },
  delete: {
    error: false,
    message: '',
  },
}

const clientById = (state = INITIAL_STATE) => ({ ...state })

const clientsList = (state = INITIAL_STATE) => ({ ...state })

const clientsListSuccess = (
  state = INITIAL_STATE,
  action: { page: number; limit: number; count: number; data: any }
) => ({
  ...state,
  listPagination: {
    currentPage: action.page,
    limit: action.limit,
    count: action.count,
  },
  list: {
    error: false,
    message: 'Success',
    data: action.data,
  },
})

const clientsListError = (state = INITIAL_STATE, action: { data: string }) => ({
  ...state,
  listPagination: INITIAL_STATE.listPagination,
  list: {
    error: true,
    message: action.data,
    data: [],
  },
})

const clientSave = (state = INITIAL_STATE) => ({ ...state })

const clientSaveSuccess = (
  state = INITIAL_STATE,
  action: { message: string; data: any; method: string }
) => ({
  ...state,
  save: {
    error: false,
    message: action.message,
    data: action.data,
    method: action.method,
  },
})

const clientSaveError = (
  state = INITIAL_STATE,
  action: { data: string; method: string }
) => ({
  ...state,
  save: {
    error: true,
    message: action.data,
    data: '',
    method: action.method,
  },
})

const clientDelete = (state = INITIAL_STATE) => ({ ...state })

const clientDeleteSuccess = (
  state = INITIAL_STATE,
  action: { data: string }
) => ({
  ...state,
  delete: {
    error: false,
    message: action.data,
  },
})

const clientDeleteError = (
  state = INITIAL_STATE,
  action: { data: string }
) => ({
  ...state,
  delete: {
    error: true,
    message: action.data,
  },
})

const clientSaveReset = (state = INITIAL_STATE) => {
  return {
    ...state,
    save: INITIAL_STATE.save,
  }
}

const clientDeleteReset = (state = INITIAL_STATE) => {
  return {
    ...state,
    delete: INITIAL_STATE.delete,
  }
}

const clientsListReset = (state = INITIAL_STATE) => {
  return {
    ...state,
    list: INITIAL_STATE.list,
    listPagination: INITIAL_STATE.listPagination,
  }
}

/**
 * Reducer
 */
export default createReducer(INITIAL_STATE, {
  [Types.CLIENT_BY_ID]: clientById,
  [Types.CLIENTS_LIST]: clientsList,
  [Types.CLIENTS_LIST_SUCCESS]: clientsListSuccess,
  [Types.CLIENTS_LIST_ERROR]: clientsListError,
  [Types.CLIENT_SAVE]: clientSave,
  [Types.CLIENT_SAVE_SUCCESS]: clientSaveSuccess,
  [Types.CLIENT_SAVE_ERROR]: clientSaveError,
  [Types.CLIENT_DELETE]: clientDelete,
  [Types.CLIENT_DELETE_SUCCESS]: clientDeleteSuccess,
  [Types.CLIENT_DELETE_ERROR]: clientDeleteError,
  [Types.CLIENTS_LIST_RESET]: clientsListReset,
  [Types.CLIENT_SAVE_RESET]: clientSaveReset,
  [Types.CLIENT_DELETE_RESET]: clientDeleteReset,
})
