import { call, put } from 'redux-saga/effects'
import { getApi } from '../../services/api'

import { Creators as ContractsActions } from '../ducks/contracts'

import { InterfaceContract } from '../../prototypes'

export function* contractsList(payload: { page: number; limit: number }) {
  const { page, limit } = payload
  let query = '?sortBy=id&order=desc'

  if (page) {
    query += `&page=${page}`
  }

  if (limit) {
    query += `&limit=${limit}`
  }

  try {
    const { data } = yield call(getApi().get, `/contracts${query}`)

    if (data.items.length > 0) {
      data.items = data.items.map((item: any) => ({
        ...item,
        clients: JSON.parse(item.clients),
      }))
    }

    const { count } = yield contractsCount()
    yield put(
      ContractsActions.contractsListSuccess(
        data.items || [],
        page,
        limit,
        count
      )
    )
  } catch (error) {
    console.log(error)
    yield put(ContractsActions.contractsListError(error.response.data))
  }
}

export function* contractById(payload: { id: number }) {
  const { id } = payload

  try {
    const { data } = yield call(getApi().get, `/contracts/${id}`)

    data.clients = JSON.parse(data.clients)

    yield put(ContractsActions.contractsListSuccess([data]))
  } catch (error) {
    console.log(error)

    if (error.response.data.toLowerCase() === 'not found') {
      window.location.href = '/'
      return
    }

    yield put(ContractsActions.contractsListError(error.response.data))
  }
}

export function* contractsCount() {
  try {
    const { data } = yield call(getApi().get, `/contracts`)
    return { count: data.count || 0 }
  } catch (error) {
    console.log(error)
    yield put(ContractsActions.contractsListError(error.response.data))
  }
}

export function* contractSave(payload: { contract: any; method: string }) {
  const { contract, method } = payload

  contract.clients = JSON.stringify(contract.clients)

  try {
    const { data } = yield call(
      method === 'update' ? getApi().put : getApi().post,
      `/contracts${method === 'update' ? `/${contract.id}` : ''}`,
      contract
    )

    const message = `O contrato ${data.title}, foi ${
      method === 'create' ? 'adicionado' : 'atualizado'
    } com sucesso.`

    yield put(ContractsActions.contractSaveSuccess(data, method, message))
    yield put(ContractsActions.contractsListReset())
  } catch (error) {
    console.log(error)

    const message = `Não foi possível ${
      method === 'create' ? 'adicionar' : 'atualizar'
    } o contrato ${contract.title}, tente novamente mais tarde.`

    yield put(ContractsActions.contractSaveError(message, method))
  }
}

export function* contractDelete(payload: { contract: InterfaceContract }) {
  const { contract } = payload

  try {
    yield call(getApi().delete, `/contracts/${contract.id}`)

    const message = `O contrato ${contract.title}, foi excluído com sucesso.`

    yield put(ContractsActions.contractDeleteSuccess(message))
    yield put(ContractsActions.contractsListReset())
  } catch (error) {
    console.log(error)

    const message = `Não foi possível excluir o contrato ${contract.title}, tente novamente mais tarde.`

    yield put(ContractsActions.contractDeleteError(message))
  }
}

export function* clientsOptions() {
  try {
    const { data } = yield call(getApi().get, `/clients?sortBy=id&order=desc`)
    yield put(ContractsActions.clientsOptionsSuccess(data.items || []))
  } catch (error) {
    console.log(error)
    yield put(ContractsActions.clientsOptionsError(error.response.data))
  }
}

export function* contractByCode(payload: { code: string }) {
  const { code } = payload

  try {
    const { data } = yield call(getApi().get, `/contracts?search=${code}`)
    let response: InterfaceContract = {}

    if (data.items.length > 0) {
      const index = data.items.findIndex(
        (item: InterfaceContract) => item.code === parseInt(code)
      )

      if (index > -1) {
        response = data.items[index]
        response.clients = JSON.parse(data.items[index].clients)
      }
    }

    yield put(ContractsActions.contractByCodeSuccess(response))
  } catch (error) {
    console.log(error)
    yield put(ContractsActions.contractByCodeError(error.response.data))
  }
}
