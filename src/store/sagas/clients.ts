import { call, put } from 'redux-saga/effects'
import { getApi } from '../../services/api'

import { Creators as ClientsActions } from '../ducks/clients'
import { Creators as ContractsActions } from '../ducks/contracts'

import { InterfaceClient } from '../../prototypes'

export function* clientsList(payload: { page: number; limit: number }) {
  const { page, limit } = payload

  let query = '?sortBy=id&order=desc'

  if (page) {
    query += `&page=${page}`
  }

  if (limit) {
    query += `&limit=${limit}`
  }

  try {
    const { data } = yield call(getApi().get, `/clients${query}`)
    const { count } = yield clientsCount()
    yield put(ClientsActions.clientsListSuccess(data.items, page, limit, count))
  } catch (error) {
    console.log(error)
    yield put(ClientsActions.clientsListError(error.response.data))
  }
}

export function* clientById(payload: { id: string }) {
  const { id } = payload
  try {
    const { data } = yield call(getApi().get, `/clients/${id}`)
    yield put(ClientsActions.clientsListSuccess([data]))
  } catch (error) {
    console.log(error)

    if (error.response.data.toLowerCase() === 'not found') {
      window.location.href = '/'
      return
    }

    yield put(ClientsActions.clientsListError(error.response.data))
  }
}

export function* clientsCount() {
  try {
    const { data } = yield call(getApi().get, `/clients`)
    return { count: data.count || 0 }
  } catch (error) {
    console.log(error)
    yield put(ClientsActions.clientsListError(error.response.data))
  }
}

export function* clientSave(payload: {
  client: InterfaceClient
  method: string
}) {
  const { client, method } = payload

  try {
    const { data } = yield call(
      method === 'update' ? getApi().put : getApi().post,
      `/clients${method === 'update' ? `/${client.id}` : ''}`,
      client
    )

    const message = `O cliente ${data.name + ' ' + data.lastName}, foi ${
      method === 'create' ? 'adicionado' : 'atualizado'
    } com sucesso.`

    yield put(ClientsActions.clientSaveSuccess(data, method, message))

    yield put(ContractsActions.clientsOptionsReset())
    yield put(ClientsActions.clientsListReset())
  } catch (error) {
    console.log(error)

    const message = `Não foi possível ${
      method === 'create' ? 'adicionar' : 'atualizar'
    } o cliente ${
      client.name + ' ' + client.lastName
    }, tente novamente mais tarde.`

    yield put(ClientsActions.clientSaveError(message, method))
  }
}

export function* clientDelete(payload: { client: InterfaceClient }) {
  const { client } = payload

  try {
    yield call(getApi().delete, `/clients/${client.id}`)

    const message = `O cliente ${
      client.name + ' ' + client.lastName
    }, foi excluído com sucesso.`

    yield put(ClientsActions.clientDeleteSuccess(message))
    yield put(ClientsActions.clientsListReset())
  } catch (error) {
    console.log(error)

    const message = `Não foi possível excluir o cliente ${
      client.name + ' ' + client.lastName
    }, tente novamente mais tarde.`

    yield put(ClientsActions.clientDeleteError(message))
  }
}
