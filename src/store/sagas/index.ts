import { all, takeLatest, ActionPattern } from 'redux-saga/effects'

import { Types as ContractsTypes } from '../ducks/contracts'
import {
  contractById,
  contractByCode,
  contractsList,
  contractSave,
  contractDelete,
  clientsOptions,
} from './contracts'

import { Types as ClientsTypes } from '../ducks/clients'
import { clientById, clientsList, clientSave, clientDelete } from './clients'

export default function* rootSaga() {
  return yield all([
    takeLatest<ActionPattern<any>>(ContractsTypes.CONTRACT_BY_ID, contractById),
    takeLatest<ActionPattern<any>>(
      ContractsTypes.CONTRACT_BY_CODE,
      contractByCode
    ),
    takeLatest<ActionPattern<any>>(
      ContractsTypes.CONTRACTS_LIST,
      contractsList
    ),
    takeLatest<ActionPattern<any>>(
      ContractsTypes.CLIENTS_OPTIONS,
      clientsOptions
    ),
    takeLatest<ActionPattern<any>>(ContractsTypes.CONTRACT_SAVE, contractSave),
    takeLatest<ActionPattern<any>>(
      ContractsTypes.CONTRACT_DELETE,
      contractDelete
    ),
    takeLatest<ActionPattern<any>>(ClientsTypes.CLIENT_BY_ID, clientById),
    takeLatest<ActionPattern<any>>(ClientsTypes.CLIENTS_LIST, clientsList),
    takeLatest<ActionPattern<any>>(ClientsTypes.CLIENT_SAVE, clientSave),
    takeLatest<ActionPattern<any>>(ClientsTypes.CLIENT_DELETE, clientDelete),
  ])
}
