import axios from 'axios'

const CLOUD_URL: string = 'https://api.cloudinary.com/v1_1'
const CLOUD_NAME: string = 'dux93mnmk'
const CLOUD_PRESET: string = 'i4j8ijzq'

export const CloudUpload = async (base64: ArrayBuffer | string | null) => {
  const { data } = await axios.post(`${CLOUD_URL}/${CLOUD_NAME}/image/upload`, {
    file: base64,
    upload_preset: CLOUD_PRESET,
  })

  return data
}
