import axios, { AxiosInstance } from 'axios'

const API_URL: string = 'https://5f08cec7445d080016691d1f.mockapi.io/api'

export const getApi = (token?: string): AxiosInstance => {
  const headers: { Accept: string; Authorization?: string } = {
    Accept: 'application/json',
  }
  if (token) {
    headers['Authorization'] = `Bearer ${token}`
  }
  return axios.create({ baseURL: API_URL, headers })
}
