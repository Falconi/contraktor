import { createMuiTheme } from '@material-ui/core'

export const ContraktorTheme = createMuiTheme({
  typography: {
    fontFamily: 'Roboto',
    fontSize: 16,
    htmlFontSize: 10,
  },
  palette: {
    // Yellow
    primary: {
      main: '#F7C544',
      contrastText: '#1E212A',
    },
    // Gray
    secondary: {
      main: '#9BA6BE',
      contrastText: '#1E212A',
    },
    // Orange
    warning: {
      main: '#F77A44',
      contrastText: '#1E212A',
    },
    // Blue
    info: {
      main: '#44ACF7',
      contrastText: '#1E212A',
    },
    // Green
    success: {
      main: '#44F7C1',
      contrastText: '#1E212A',
    },
    // Red
    error: {
      main: '#F74444',
      contrastText: '#FFF',
    },
    background: {
      // Dark gray (Most use cases)
      paper: '#2F3440',
      // Dark black
      default: '#1E212A',
    },
    text: {
      // Light gray
      primary: '#EBF1FF',
      // Gray
      secondary: '#9BA6BE',
    },
    tonalOffset: 0.2,
  },
})
