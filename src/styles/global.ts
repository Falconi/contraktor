import { withStyles } from '@material-ui/core/styles'

export const GlobalStyle = withStyles({
  '@global': {
    html: {
      position: 'relative',
      minHeight: '100%',
      fontFamily: 'Roboto',
      fontWeight: 500,
      fontSize: '10px',
    },
    body: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      minHeight: '100%',
      fontFamily: 'Roboto',
      fontWeight: 500,
      fontSize: '1.6rem',
    },
    '#root': {
      display: 'flex',
      flexDirection: 'column',
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      minHeight: '100%',
    },
    '.defaultContainer': {
      display: 'block',
      margin: '0 auto',
      width: '100%',
      maxWidth: '117rem',
      padding: '0 15px',
    },
    'main.defaultContainer': {
      paddingTop: '5rem',
      paddingBottom: '20rem',
    },
    '.column': {
      width: '100%',
      padding: '1.5rem',
      '&.col-6': {
        maxWidth: '50%',
      },
      '&.col-4': {
        maxWidth: '33.33333%',
      },
    },
    a: {
      textDecoration: 'none',
      color: 'inherit',
    },
    'p, span, a, h1, h2, h3, h4, h5, h6, ul, ol, li': {
      margin: 0,
      padding: 0,
    },
    'ul, ol': {
      listStyle: 'none',
    },
    '@media(max-width: 1450px)': {
      html: {
        fontSize: '9px',
      },
    },
    '@media(max-width: 767px)': {
      html: {
        fontSize: '8px',
      },
      '.column': {
        '&.col-6': {
          maxWidth: '100%',
        },
        '&.col-4': {
          maxWidth: '100%',
        },
        '&.col-xs-6': {
          maxWidth: '50%',
        },
      },
      'main.defaultContainer': {
        paddingBottom: '10rem',
      },
    },
  },
})(() => null)
