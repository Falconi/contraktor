export interface InterfaceClient {
  [id: string]: any
  contractId?: string
  createdAt?: string
  name?: string
  lastName?: string
  cpf?: string
  contactNumber?: string
  email?: string
}

export interface InterfaceContract {
  [id: string]: any
  createdAt?: string
  code?: number
  title?: string
  initialDate?: string
  finalDate?: string
  file?: string
  clients?: Array<InterfaceClient>
}

export interface InterfaceClientsInitalState {
  listPagination: {
    currentPage: number
    limit: number
    count: number
  }
  list: {
    error: boolean
    message: string
    data: any
  }
  save: {
    error: boolean
    message: string
    data: any
    method: string
  }
  delete: {
    error: boolean
    message: string
  }
}

export interface InterfaceContractsInitalState {
  listPagination: {
    currentPage: number
    limit: number
    count: number
  }
  list: {
    error: boolean
    message: string
    data: any
  }
  save: {
    error: boolean
    message: string
    data: any
    method: string
  }
  delete: {
    error: boolean
    message: string
  }
  clientsOptions: {
    error: boolean
    message: string
    data: any
  }
  details: {
    error: boolean
    message: string
    data: any
  }
}

export interface InterfaceStoreContract {
  error?: boolean
  message?: string
  data?: Array<InterfaceContract>
}

export interface InterfaceFormField {
  require: boolean
  value?: any
}

export interface InterfaceFormContract {
  [title: string]: InterfaceFormField
  file: InterfaceFormField
  initialDate: InterfaceFormField
  finalDate: InterfaceFormField
  clients: InterfaceFormField
}

export type TypeFeedback = 'error' | 'default' | 'success' | 'warning' | 'info'

export interface InterfaceStoreClient {
  error?: boolean
  message?: string
  data?: Array<InterfaceClient>
}

export interface InterfaceFormClient {
  [name: string]: InterfaceFormField
  lastName: InterfaceFormField
  cpf: InterfaceFormField
  contactNumber: InterfaceFormField
  email: InterfaceFormField
}

export interface InterfaceConfirmDialog {
  show: boolean
  title: string
  description: string
  onConfirm: () => void
  onCancel: () => void
}

export interface InterfaceButton {
  children?: React.ReactNode
  loading?: boolean
  disabled?: boolean
  backgroundColor?: string
  color?: string
  fontSize?: string
  width?: string
  height?: string
  className?: string
  type: string
  component?: any
  onClick?: () => void
  style?: any
  rest?: any[]
}

export interface InterfaceInput {
  label?: string
  value?: string | number
  containerClasses?: {}
  inputClasses?: {}
  labelColor?: string
  inputBackgroundColor?: string
  inputTextColor?: string
  inputTextSize?: string
  helpTextSize?: string
  helperText?: string | number
  disabled?: boolean
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void
  error?: boolean
  rest?: any[]
}

export interface InterfaceInputDate {
  type?: string
  locale?: string
  value?: any
  onChange?: (value: any) => void
  disabled?: boolean
  width?: string
  height?: string
  backgroundColor?: string
  borderColor?: string
  color?: string
  fontSize?: string
}

export interface InterfaceSearch {
  placeholder?: string
  search?: string | number
  setSearch?: (value: string | number) => void
  loading?: boolean
  disabled?: boolean
  handleDoSearch?: (search: string | number) => void
  handleResetSearch?: () => void
  borderColor?: string
  backgroundColor?: string
  fontSize?: string
  color?: string
  placeholderColor?: string
  width?: string
  height?: string
  classesCustom?: {}
}

export interface InterfaceModal {
  show: boolean
  handleClose: () => void
  children?: React.ReactNode
}

export interface InterfacePageTitle {
  title: string
  description: string
  navigationTitle?: string
  navigationLink?: any
}

export type TypeAlign = 'inherit' | 'left' | 'center' | 'right' | 'justify'

export interface InterfaceTableColumn {
  id: string | number
  label: string
  minWidth?: number | string
  align?: TypeAlign
}

export interface InterfaceTable {
  columns: Array<InterfaceTableColumn>
  rows: Array<any>
  rowsPerPage?: number
  page?: number
  count?: number
  loading?: boolean
  onRowsPerPageChange?: (value: number) => void
  onPageChange?: (value: number) => void
}

export interface InterfaceSelectMultiples {
  options: Array<{ title: string | number; value: any }>
  label?: string
  placeholder?: string
  value?: any
  onChange?: (options: Array<{ title: string | number; value: any }>) => void
}
