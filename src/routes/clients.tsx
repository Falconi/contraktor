import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import List from '../pages/Clients'
import Create from '../pages/Clients/Create'

const RoutesClients: React.FunctionComponent = (): JSX.Element => {
  return (
    <Switch>
      <Route exact path="/clientes" component={List} />
      <Route exact path="/clientes/:id" component={Create} />
      <Redirect from="*" to="/clientes" />
    </Switch>
  )
}

export default RoutesClients
