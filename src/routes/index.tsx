import React from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import { ScrollTop } from '../utils/router-scroll-top'

import RoutesClients from './clients'
import RoutesContracts from './contracts'

import Home from '../pages/Home'

import Header from '../components/Header'
import Footer from '../components/Footer'

const Routes: React.FunctionComponent = (): JSX.Element => {
  return (
    <BrowserRouter>
      <ScrollTop>
        <Header />
        <main className="defaultContainer">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/contratos" component={RoutesContracts} />
            <Route path="/clientes" component={RoutesClients} />
            <Redirect from="*" to="/" />
          </Switch>
        </main>
        <Footer />
      </ScrollTop>
    </BrowserRouter>
  )
}

export default Routes
