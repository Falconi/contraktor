import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import List from '../pages/Contracts'
import Create from '../pages/Contracts/Create'

const RoutesClients: React.FunctionComponent = (): JSX.Element => {
  return (
    <Switch>
      <Route exact path="/contratos" component={List} />
      <Route exact path="/contratos/:id" component={Create} />
      <Redirect from="*" to="/contratos" />
    </Switch>
  )
}

export default RoutesClients
