import { useEffect } from 'react'
import { useLocation } from 'react-router-dom'

export const ScrollTop: React.FunctionComponent<React.ReactNode> = ({
  children,
}): React.ReactElement => {
  const { pathname } = useLocation()

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [pathname])

  return children as React.ReactElement
}
