import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeUseStyles = () => Record<'root' | 'container' | 'loading', string>

export const useStyles: TypeUseStyles = makeStyles<Theme>((theme: Theme) =>
  createStyles({
    root: {
      position: 'relative',
      width: '100%',
      '& .MuiSvgIcon-root ': {
        color: theme.palette.primary.main,
      },
      '& .MuiButtonBase-root': {
        '&.Mui-disabled': {
          '& .MuiSvgIcon-root ': {
            opacity: 0.5,
          },
        },
      },
      '& .MuiTableCell-stickyHeader': {
        padding: '1rem',
        backgroundColor: theme.palette.background.paper,
        fontSize: '2rem',
        borderColor: theme.palette.text.secondary,
        color: theme.palette.text.secondary,
      },
      '& .MuiTableCell-body': {
        padding: '1rem',
        backgroundColor: theme.palette.background.default,
        fontSize: '1.6rem',
        borderColor: theme.palette.text.secondary,
        color: theme.palette.text.primary,
      },
      '& .MuiTablePagination-root': {
        '& .MuiToolbar-root': {
          padding: '0 0 0 1.5rem',
          color: theme.palette.text.secondary,
          '& .MuiTablePagination-input': {
            marginRight: '1rem',
          },
          '& .MuiTablePagination-actions': {
            marginLeft: 0,
          },
        },
      },
    },
    container: {
      maxHeight: '70rem',
    },
    loading: {
      zIndex: 999999999,
      position: 'absolute',
      top: 0,
      left: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      height: '100%',
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
  })
)
