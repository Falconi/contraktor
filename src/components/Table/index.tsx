import React from 'react'

import { InterfaceTable, InterfaceTableColumn } from '../../prototypes'

import { useStyles } from './styles'
import Paper from '@material-ui/core/Paper'
import {
  LabelDisplayedRowsArgs,
  Table as MaterialTable,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  CircularProgress,
} from '@material-ui/core'

const Table: React.FunctionComponent<InterfaceTable> = ({
  columns,
  rows,
  rowsPerPage = 10,
  page = 0,
  count = 0,
  loading = false,
  onRowsPerPageChange = value => {},
  onPageChange = value => {},
}): JSX.Element => {
  const classes = useStyles()

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    page: number
  ): void => {
    onPageChange(page)
  }

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<{ value: any }>
  ): void => {
    onRowsPerPageChange(event.target.value)
  }

  const paginationDisplay: React.FunctionComponent<LabelDisplayedRowsArgs> = ({
    count,
    from,
    page,
    to,
  }) => (
    <span>
      {from} - {to} de {count}
    </span>
  )

  return (
    <Paper className={classes.root}>
      {loading && (
        <div className={classes.loading}>
          <CircularProgress size={50} />
        </div>
      )}
      <TableContainer className={classes.container}>
        <MaterialTable stickyHeader>
          <TableHead>
            <TableRow>
              {columns.map((column: InterfaceTableColumn) => (
                <TableCell
                  key={`column-${column.id}`}
                  style={{ minWidth: column.minWidth || '' }}
                  align={column.align || 'left'}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row, rowNumber: number) => {
              return (
                <TableRow
                  hover
                  role="checkbox"
                  tabIndex={-1}
                  key={`row-${rowNumber}`}
                >
                  {columns.map((column: InterfaceTableColumn) => {
                    const value = row[column.id]
                    return (
                      <TableCell
                        key={`row-${rowNumber}-column-${column.id}`}
                        align={column.align || 'left'}
                      >
                        {value}
                      </TableCell>
                    )
                  })}
                </TableRow>
              )
            })}
          </TableBody>
        </MaterialTable>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={count}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
        labelRowsPerPage="Por Página"
        labelDisplayedRows={(paginationInfo: LabelDisplayedRowsArgs) =>
          paginationDisplay(paginationInfo)
        }
      />
    </Paper>
  )
}

export default Table
