import React from 'react'

import { InterfaceButton } from '../../../prototypes'

import { useStyles } from './styles'
import { Button as MaterialButton, CircularProgress } from '@material-ui/core'

const Button: React.FunctionComponent<InterfaceButton> = ({
  children,
  loading = false,
  disabled = false,
  backgroundColor = '#F7C544',
  color = '#151820',
  fontSize = '1.6rem',
  width = '15rem',
  height = '4rem',
  className,
  type,
  component,
  style,
  onClick,
  ...rest
}): JSX.Element => {
  const classes = useStyles({
    fontSize,
    backgroundColor,
    color,
    width,
    height,
    loading,
  })

  return (
    <MaterialButton
      type={type}
      href={''}
      disabled={loading || disabled}
      className={`${classes.root} ${className || ''}`}
      onClick={onClick}
      component={component}
      style={style}
      {...rest}
    >
      {loading ? (
        <CircularProgress
          className="loading"
          style={{
            width: `${(parseFloat(height.replace('rem', '')) / 100) * 70}rem`,
            height: `${(parseFloat(height.replace('rem', '')) / 100) * 70}rem`,
          }}
        />
      ) : (
        children
      )}
    </MaterialButton>
  )
}

export default Button
