import { makeStyles, darken, Theme, createStyles } from '@material-ui/core'

type TypeButton = {
  width: string
  height: string
  backgroundColor: string
  fontSize: string
  color: string
  loading: boolean
}
type TypeUseStyles = (props: TypeButton) => Record<'root', string>

export const useStyles: TypeUseStyles = makeStyles<Theme, TypeButton>(() =>
  createStyles({
    root: {
      width: '100%',
      maxWidth: props => props.width,
      height: props => props.height,
      padding: '0 1.5rem',
      backgroundColor: props => props.backgroundColor + '!important',
      fontWeight: 500,
      fontSize: props => props.fontSize,
      lineHeight: '1.6rem',
      textTransform: 'none',
      color: props => props.color,
      opacity: props => (props.loading ? 0.5 : 1),
      '&:hover': {
        backgroundColor: props =>
          darken(props.backgroundColor, 0.23) + '!important',
      },
      '& .loading': {
        color: props => props.color,
      },
    },
  })
)
