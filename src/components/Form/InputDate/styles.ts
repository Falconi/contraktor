import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeInputDate = {
  width: string
  height: string
  backgroundColor: string
  fontSize: string
  color: string
  borderColor: string
}
type TypeUseStyles = (props: TypeInputDate) => Record<'root', string>

export const useStyles: TypeUseStyles = makeStyles<Theme, TypeInputDate>(
  (theme: Theme) =>
    createStyles({
      root: {
        width: props => props.width,
        '& .MuiInputBase-root': {
          height: props => props.height,
          padding: '0 0 0 1.5rem',
          backgroundColor: props => props.backgroundColor,
          fontWeight: 500,
          fontSize: props => props.fontSize,
          lineHeight: props => props.height,
          color: props => props.color,
          '& .MuiOutlinedInput-notchedOutline': {
            borderWidth: '1px',
            borderColor: props => props.borderColor,
          },
          '& input': {
            padding: '0 3.5rem 0 0',
          },
          '&.Mui-disabled': {
            cursor: 'not-allowed',
            '& input': {
              cursor: 'not-allowed',
              opacity: 0.35,
            },
            '& button': {
              cursor: 'not-allowed',
              opacity: 0.35,
            },
          },
          '& .MuiSvgIcon-root': {
            color: props => props.color,
          },
        },
        '& .MuiButtonBase-root': {
          position: 'absolute',
          top: 0,
          right: 0,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-end',
          width: '100%',
          height: '100%',
          padding: '0 1.3rem',
          '& .MuiIconButton-label': {
            width: 'auto',
            '& .MuiSvgIcon-root': {
              width: '2rem',
              height: '2rem',
              flexShrink: 0,
            },
          },
        },
      },
    })
)
