import React from 'react'

import { InterfaceInputDate } from '../../../prototypes'

import Moment from 'moment'
import MomentUtils from '@date-io/moment'
import 'moment/locale/pt-br'

import { useStyles } from './styles'

import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers'

const InputDate: React.FunctionComponent<InterfaceInputDate> = ({
  type = 'date',
  locale = 'pt_BR',
  value = new Date(),
  onChange = value => {},
  disabled = false,
  width = '',
  height = '4rem',
  backgroundColor = '#2F3440',
  borderColor = '#6A7180',
  color = '#EBF1FF',
  fontSize = '1.6rem',
}): JSX.Element => {
  const classes = useStyles({
    width: !width ? (locale === 'pt_BR' ? '19rem' : '20.7rem') : width,
    height,
    borderColor,
    color,
    fontSize,
    backgroundColor: disabled ? '#1E212A' : backgroundColor,
  })

  return (
    <MuiPickersUtilsProvider
      libInstance={Moment}
      utils={MomentUtils}
      locale={locale}
    >
      {type === 'date' && (
        <KeyboardDatePicker
          className={classes.root}
          value={value}
          onChange={(value: any) => onChange(value)}
          format="L"
          cancelLabel="Cancelar"
          okLabel="Confirmar"
          todayLabel="Hoje"
          showTodayButton={true}
          inputVariant="outlined"
          disabled={disabled}
        />
      )}
    </MuiPickersUtilsProvider>
  )
}

export default InputDate
