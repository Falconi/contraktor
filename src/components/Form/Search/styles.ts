import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeInputSearch = {
  width: string
  height: string
  backgroundColor: string
  fontSize: string
  color: string
  borderColor: string
  placeholderColor: string
}
type TypeUseStyles = (
  props: TypeInputSearch
) => Record<'root' | 'input' | 'searchIcon' | 'iconButton' | 'loading', string>

export const useStyles: TypeUseStyles = makeStyles<Theme, TypeInputSearch>(() =>
  createStyles({
    root: {
      padding: '0 1.3rem 0 1.5rem',
      display: 'flex',
      alignItems: 'center',
      height: props => props.height,
      width: props => props.width,
      backgroundColor: props => props.backgroundColor,
      borderColor: props => props.borderColor,
      borderWidth: 1,
      borderStyle: 'solid',
    },
    input: {
      margin: 0,
      paddingRight: '1.5rem',
      flex: 1,
      color: props => props.color,
      fontSize: props => props.fontSize,
      fontWeight: 500,
      '& ::placeholder': {
        opacity: 1,
        color: props => props.placeholderColor,
      },
      '& .Mui-disabled': {
        opacity: 0.35,
        cursor: 'not-allowed',
        color: props => props.placeholderColor,
      },
    },
    searchIcon: {
      fontSize: props => `calc(${props.fontSize} + 0.5rem)`,
      color: props => props.color,
      '&[disabled]': {
        opacity: 0.35,
        cursor: 'not-allowed',
      },
    },
    iconButton: {
      padding: 0,
      color: props => props.color,
      '&.Mui-disabled': {
        opacity: 0.35,
        color: props => props.color,
      },
    },
    loading: {
      color: props => props.placeholderColor,
    },
  })
)
