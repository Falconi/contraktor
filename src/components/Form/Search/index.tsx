import React from 'react'

import { InterfaceSearch } from '../../../prototypes'

import {
  Paper,
  InputBase,
  IconButton,
  CircularProgress,
} from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import CloseIcon from '@material-ui/icons/Close'

import clsx from 'clsx'
import { useStyles } from './styles'

const InputSearch: React.FunctionComponent<InterfaceSearch> = ({
  placeholder = 'Buscar',
  search = '',
  setSearch = (value: string | number): void => {},
  loading = false,
  disabled = false,
  handleDoSearch = (search: string | number): void => {},
  handleResetSearch = () => {},
  borderColor = '#6A7180',
  backgroundColor = '#2F3440',
  fontSize = '1.6rem',
  color = '#EBF1FF',
  placeholderColor = '#9BA6BE',
  width = '20rem',
  height = '3.3rem',
  classesCustom = {},
}): JSX.Element => {
  const classes = useStyles({
    width: '100%',
    height,
    borderColor,
    backgroundColor: disabled ? '#1E212A' : backgroundColor,
    fontSize,
    color,
    placeholderColor,
  })

  const submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    handleDoSearch(search)
  }

  return (
    <form onSubmit={submit} style={{ width }}>
      <Paper className={clsx(classes.root, classesCustom)}>
        <InputBase
          className={classes.input}
          placeholder={placeholder}
          inputProps={{ 'aria-label': placeholder }}
          value={search}
          onChange={el => setSearch(el.target.value)}
          disabled={disabled}
        />

        {!search && !loading && <SearchIcon className={classes.searchIcon} />}

        {search && !loading && (
          <IconButton
            type="button"
            className={classes.iconButton}
            aria-label="search"
            onClick={handleResetSearch}
            disabled={disabled}
          >
            <CloseIcon style={{ width: `calc(${fontSize} + 0.5rem)` }} />
          </IconButton>
        )}

        {loading && (
          <CircularProgress
            className={classes.loading}
            style={{
              width: `${(parseFloat(height.replace('rem', '')) / 100) * 50}rem`,
              height: `${
                (parseFloat(height.replace('rem', '')) / 100) * 50
              }rem`,
            }}
          />
        )}
      </Paper>
    </form>
  )
}

export default InputSearch
