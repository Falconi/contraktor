import React from 'react'
import clsx from 'clsx'

import { InterfaceInput } from '../../../prototypes'

import { TextField } from '@material-ui/core'

import { useStyles } from './styles'

const Input: React.FunctionComponent<InterfaceInput> = ({
  label = '',
  value = '',
  containerClasses = {},
  inputClasses = {},
  labelColor = '#6A7180',
  inputBackgroundColor = '#2F3440',
  inputTextColor = '#EBF1FF',
  inputTextSize = '1.4rem',
  helpTextSize = '1.4rem',
  helperText = '',
  disabled = false,
  onChange,
  error = false,
  ...rest
}): JSX.Element => {
  const classes = useStyles({
    labelColor,
    inputBackgroundColor: disabled ? '#1E212A' : inputBackgroundColor,
    inputTextColor,
    inputTextSize,
    helpTextSize,
  })
  const inputDefaultClasses = label
    ? classes.input
    : clsx(classes.input, classes.inputNoLabel)
  return (
    <div className={clsx(classes.root, containerClasses)}>
      <TextField
        label={label}
        className={clsx(inputDefaultClasses, inputClasses)}
        variant="outlined"
        fullWidth
        disabled={disabled}
        value={value}
        helperText={helperText}
        onChange={onChange}
        error={error}
        {...rest}
      />
    </div>
  )
}

export default Input
