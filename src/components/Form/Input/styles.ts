import { makeStyles, lighten, Theme, createStyles } from '@material-ui/core'

type TypeInput = {
  inputBackgroundColor: string
  labelColor: string
  inputTextColor: string
  inputTextSize: string
  helpTextSize: string
}
type TypeUseStyles = (
  props: TypeInput
) => Record<'root' | 'input' | 'inputNoLabel', string>

export const useStyles: TypeUseStyles = makeStyles<Theme, TypeInput>(() =>
  createStyles({
    root: {
      width: '100%',
      margin: '0',
      '& legend': {
        fontSize: '1.4rem',
      },
    },
    input: {
      '& .MuiInputBase-root': {
        backgroundColor: props => props.inputBackgroundColor,
      },
      '& .MuiFormLabel-root': {
        color: props => props.labelColor,
      },
      '& .MuiFormLabel-root.Mui-focused': {
        color: props => lighten(props.labelColor, 0.1),
      },
      '& .MuiInputLabel-outlined.MuiInputLabel-shrink': {
        color: props => lighten(props.labelColor, 0.1),
      },
      '& .MuiOutlinedInput-notchedOutline': {
        borderColor: props => props.labelColor,
      },
      '& .MuiOutlinedInput-root': {
        color: props => props.inputTextColor,
        fontSize: props => props.inputTextSize,
      },
      '& .Mui-focused, & .MuiOutlinedInput-root:hover': {
        '& .MuiOutlinedInput-notchedOutline': {
          borderColor: props => props.labelColor,
          borderWidth: 1,
        },
      },
      '& .MuiFormHelperText-root': {
        fontSize: props => props.helpTextSize,
        marginLeft: 0,
        marginRight: 0,
      },
      '& .Mui-disabled': {
        '& .MuiInputBase-input': {
          opacity: 0.35,
          cursor: 'not-allowed',
        },
        '& .MuiOutlinedInput-notchedOutline': {
          borderColor: props => props.labelColor,
        },
      },
    },
    inputNoLabel: {
      '& .Mui-focused': {
        '& .MuiOutlinedInput-notchedOutline': {
          '& legend span': {
            paddingLeft: 0,
            paddingRight: 0,
          },
        },
      },
      '& .MuiOutlinedInput-notchedOutline': {
        '& legend span': {
          paddingLeft: 0,
          paddingRight: 0,
        },
      },
    },
  })
)
