import React from 'react'

import { InterfaceSelectMultiples } from '../../../prototypes'

import { TextField } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'

import { useStyles } from './styles'

const SelectMultiples: React.FunctionComponent<InterfaceSelectMultiples> = ({
  options,
  label,
  placeholder,
  value,
  onChange = (options: Array<{ title: string | number; value: any }>) => {},
}) => {
  const classes = useStyles({})

  return (
    <Autocomplete
      className={classes.root}
      multiple
      id="select-multiples"
      options={options}
      getOptionLabel={(option: any) => option.title}
      value={value}
      filterSelectedOptions
      onChange={(
        event: React.ChangeEvent<{}>,
        value: Array<{ title: string | number; value: any }>
      ) => onChange(value)}
      renderInput={(params: any) => (
        <TextField
          {...params}
          variant="outlined"
          label={label}
          placeholder={placeholder}
          className={classes.input}
        />
      )}
    />
  )
}

export default SelectMultiples
