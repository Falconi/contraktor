import { makeStyles, Theme, lighten, createStyles } from '@material-ui/core'

type TypeInputSearch = {}
type TypeUseStyles = (
  props: TypeInputSearch
) => Record<'root' | 'input', string>

export const useStyles: TypeUseStyles = makeStyles<Theme, TypeInputSearch>(
  (theme: Theme) =>
    createStyles({
      root: {
        '& legend': {
          fontSize: '1.4rem',
        },
        '& .MuiChip-root': {
          backgroundColor: theme.palette.background.default,
          color: theme.palette.text.primary,
        },
      },
      input: {
        '& svg': {
          color: theme.palette.text.primary,
        },
        '& .MuiInputBase-root': {
          backgroundColor: '#2F3440',
        },
        '& .MuiFormLabel-root': {
          color: '#6A7180',
        },
        '& .MuiFormLabel-root.Mui-focused': {
          color: lighten('#6A7180', 0.1),
        },
        '& .MuiInputLabel-outlined.MuiInputLabel-shrink': {
          color: lighten('#6A7180', 0.1),
        },
        '& .MuiOutlinedInput-notchedOutline': {
          borderColor: '#6A7180',
        },
        '& .MuiOutlinedInput-root': {
          color: '#EBF1FF',
          fontSize: '1.4rem',
        },
        '& .Mui-focused, & .MuiOutlinedInput-root:hover': {
          '& .MuiOutlinedInput-notchedOutline': {
            borderColor: '#6A7180',
            borderWidth: 1,
          },
        },
        '& .MuiFormHelperText-root': {
          fontSize: '1.4rem',
          marginLeft: 0,
          marginRight: 0,
        },
        '& .Mui-disabled': {
          '& .MuiInputBase-input': {
            opacity: 0.35,
            cursor: 'not-allowed',
          },
          '& .MuiOutlinedInput-notchedOutline': {
            borderColor: '#6A7180',
          },
        },
      },
    })
)
