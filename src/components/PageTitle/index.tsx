import React from 'react'
import { Link } from 'react-router-dom'

import ArrowIcon from '@material-ui/icons/ArrowBack'

import { InterfacePageTitle } from '../../prototypes'

import { useStyles } from './styles'

const PageTitle: React.FunctionComponent<InterfacePageTitle> = ({
  title,
  description,
  navigationLink,
  navigationTitle,
}): JSX.Element => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      {navigationLink && navigationTitle && (
        <Link to={navigationLink}>
          <ArrowIcon />
          {navigationTitle}
        </Link>
      )}
      <h1>{title}</h1>
      <p>{description}</p>
    </div>
  )
}

export default PageTitle
