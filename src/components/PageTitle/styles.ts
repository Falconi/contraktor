import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeUseStyles = () => Record<'root', string>

export const useStyles: TypeUseStyles = makeStyles<Theme>((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      '& h1': {
        fontSize: '4.8rem',
      },
      '& p': {
        fontSize: '2.4rem',
      },
      '& a': {
        display: 'flex',
        alignItems: 'center',
        marginBottom: '3rem',
        color: theme.palette.text.secondary,
        '& svg': {
          marginRight: '1rem',
          color: theme.palette.text.secondary,
        },
      },
    },
    '@media(max-width: 767px)': {
      root: {
        '& h1': {
          fontSize: '3.2rem',
        },
        '& p': {
          fontSize: '2rem',
        },
      },
    },
  })
)
