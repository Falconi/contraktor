import React from 'react'

import { InterfaceConfirmDialog } from '../../prototypes'

import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  useTheme,
} from '@material-ui/core'

import { FormButton } from '../../components/Form'

const ConfirmDialog: React.FunctionComponent<InterfaceConfirmDialog> = ({
  show,
  title,
  description,
  onConfirm,
  onCancel,
}): JSX.Element => {
  const theme = useTheme()

  return (
    <Dialog
      open={show}
      onClose={onCancel}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {description}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            width: '100%',
            padding: '8px 16px',
          }}
        >
          <FormButton
            type="button"
            width="20rem"
            height="4rem"
            fontSize="2rem"
            backgroundColor={theme.palette.success.main}
            onClick={onCancel}
            style={{ marginRight: '1.5rem' }}
          >
            Cancelar
          </FormButton>
          <FormButton
            type="button"
            width="20rem"
            height="4rem"
            fontSize="2rem"
            backgroundColor={theme.palette.error.main}
            onClick={onConfirm}
            style={{ marginLeft: '1.5rem' }}
          >
            Confirmar
          </FormButton>
        </div>
      </DialogActions>
    </Dialog>
  )
}

export default ConfirmDialog
