import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeUseStyles = () => Record<'root', string>

export const useStyles: TypeUseStyles = makeStyles<Theme>((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      marginTop: 'auto',
      padding: '2.5rem 0',
      backgroundColor: theme.palette.background.paper,
      '& .content': {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
        '& p': {
          margin: '0.5rem 0',
          textAlign: 'center',
        },
      },
    },
  })
)
