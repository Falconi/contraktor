import React from 'react'

import { useStyles } from './styles'

const Footer: React.FunctionComponent = (): JSX.Element => {
  const classes = useStyles()
  return (
    <footer className={classes.root}>
      <div className="defaultContainer">
        <div className="content">
          <p>Made with ⚙ by Contraktor in Brazil</p>
          <p>© 2020 Falcons Inc :D</p>
        </div>
      </div>
    </footer>
  )
}

export default Footer
