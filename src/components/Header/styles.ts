import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeUseStyles = () => Record<'root' | 'logo' | 'user' | 'menu', string>

export const useStyles: TypeUseStyles = makeStyles<Theme>((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      padding: '2.5rem 0',
      backgroundColor: theme.palette.background.paper,
      '& .content': {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
      },
    },
    logo: {
      width: '100%',
      maxWidth: '20rem',
    },
    user: {
      display: 'flex',
      alignItems: 'center',
      '& img': {
        width: '5rem',
        height: '5rem',
        marginLeft: '2rem',
        borderRadius: '100%',
        objectFit: 'cover',
      },
    },
    menu: {
      display: 'flex',
      alignItems: 'center',
      padding: '0 5rem',
      '& li': {
        margin: '0 2.5rem',
        '& a': {
          fontSize: '2rem',
          transition: 'all 0.3s ease',
          '&.selected': {
            color: theme.palette.info.main,
          },
        },
        '&:first-child': {
          marginLeft: 0,
        },
        '&:last-child': {
          marginRight: 0,
        },
      },
    },
    '@media(max-width: 991px)': {
      logo: {
        maxWidth: '18rem',
      },
      menu: {
        padding: '0 2rem',
        '& li': {
          margin: '0 1.5rem',
          '& a': {
            fontSize: '1.7rem',
          },
        },
      },
    },
    '@media(max-width: 767px)': {
      root: {
        '& .content': {
          flexWrap: 'wrap',
        },
      },
      '& a': {
        order: 1,
      },
      logo: {
        maxWidth: '15rem',
      },
      user: {
        order: 2,
        '& span': {
          fontSize: '1.4rem',
        },
        '& img': {
          width: '4rem',
          height: '4rem',
          marginLeft: '1rem',
        },
      },
      menu: {
        order: 3,
        justifyContent: 'center',
        width: '100%',
        marginTop: '2.5rem',
        padding: 0,
        '& li': {
          margin: '0 1.5rem',
          '& a': {
            fontSize: '1.6rem',
          },
        },
      },
    },
  })
)
