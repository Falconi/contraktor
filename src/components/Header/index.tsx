import React from 'react'
import { Link, NavLink } from 'react-router-dom'

import LogoImg from '../../assets/images/contraktor.png'
import AvatarImg from '../../assets/images/avatar.jpg'

import { useStyles } from './styles'

const Header: React.FunctionComponent = (): JSX.Element => {
  const classes = useStyles()
  return (
    <header className={classes.root}>
      <div className="defaultContainer">
        <div className="content">
          <Link to="/">
            <img src={LogoImg} alt="Logo Contraktor" className={classes.logo} />
          </Link>
          <ul className={classes.menu}>
            <li>
              <NavLink exact to="/" activeClassName="selected">
                Consultas
              </NavLink>
            </li>
            <li>
              <NavLink to="/contratos" activeClassName="selected">
                Meus Contratos
              </NavLink>
            </li>
            <li>
              <NavLink to="/clientes" activeClassName="selected">
                Meus Clientes
              </NavLink>
            </li>
          </ul>
          <div className={classes.user}>
            <span>Ipd Laboratórios</span>
            <img src={AvatarImg} alt="Ipd Laboratórios" />
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header
