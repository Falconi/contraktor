import React from 'react'

import { InterfaceClient } from '../../prototypes'

import { useStyles } from './styles'

import clsx from 'clsx'

const ClientList: React.FunctionComponent<{
  list?: Array<InterfaceClient>
}> = ({ list }): JSX.Element => {
  const classes = useStyles()

  return (
    <div className={clsx(classes.root, 'clientList')}>
      <div className={clsx(classes.header, 'clientListHeader')}>
        <span>Clientes vinculados ao contrato</span>
      </div>
      <div className={clsx(classes.body, 'clientListBody')}>
        {list?.map((client: InterfaceClient) => (
          <div className={clsx(classes.box, 'clientListBox')} key={client.id}>
            <p>
              Nome: <span className="colorSecondary">{client.name}</span>
            </p>
            <p>
              Sobrenome:{' '}
              <span className="colorSecondary">{client.lastName}</span>
            </p>
            <p>
              E-mail: <span className="colorSecondary">{client.email}</span>
            </p>
            <p>
              CPF: <span className="colorSecondary">{client.cpf}</span>
            </p>
            <p>
              Número de Contato:{' '}
              <span className="colorSecondary">{client.contactNumber}</span>
            </p>
          </div>
        ))}
      </div>
    </div>
  )
}

export default ClientList
