import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeUseStyles = () => Record<'root' | 'header' | 'body' | 'box', string>

export const useStyles: TypeUseStyles = makeStyles<Theme>((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      '& .colorSecondary': {
        color: theme.palette.text.secondary,
      },
    },
    header: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      minHeight: '4rem',
      padding: '0rem 7rem 0rem 1.5rem',
      backgroundColor: theme.palette.background.default,
      borderTopLeftRadius: '8px',
      borderTopRightRadius: '8px',
      '& span': {
        fontSize: '1.8rem',
        lineHeight: '1.8rem',
        color: theme.palette.secondary.main,
      },
    },
    body: {
      display: 'flex',
      flexDirection: 'column',
      padding: '3rem 1.5rem',
      borderBottomLeftRadius: '8px',
      borderBottomRightRadius: '8px',
    },
    box: {
      display: 'flex',
      flexDirection: 'column',
      marginBottom: '3rem',
      paddingBottom: '3rem',
      borderBottom: `1px solid ${theme.palette.secondary.main}`,
      '& p': {
        marginBottom: '0.5rem',
        '& span': {
          marginLeft: '0.5rem',
        },
        '&:last-child': {
          marginBottom: 0,
        },
      },
      '&:last-child': {
        marginBottom: 0,
        paddingBottom: 0,
        border: 0,
      },
    },
  })
)
