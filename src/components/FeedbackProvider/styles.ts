import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeUseStyles = () => Record<
  | 'root'
  | 'feedbackSuccess'
  | 'feedbackError'
  | 'feedbackWarning'
  | 'feedbackInfo'
  | 'buttonAction',
  string
>

export const useStyles: TypeUseStyles = makeStyles<Theme>(() =>
  createStyles({
    root: {
      '& .MuiPaper-root': {
        paddingLeft: '12px',
        '& .MuiSnackbarContent-message': {
          marginRight: '2rem',
          fontSize: '1.4rem',
          '& .MuiSvgIcon-root': {
            marginRight: '1.2rem!important',
            width: '2.5rem',
            height: '2.5rem',
            opacity: '1!important',
          },
        },
      },
    },
    feedbackSuccess: {},
    feedbackError: {},
    feedbackWarning: {},
    feedbackInfo: {},
    buttonAction: {
      position: 'absolute',
      top: '0.3rem',
      right: '0.3rem',
      padding: '0.3rem',
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      '&:hover': {
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
      },
      '& .MuiSvgIcon-root': {
        width: '1.7rem',
        height: '1.7rem',
        fill: '#FFF',
      },
    },
  })
)
