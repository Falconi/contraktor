import React from 'react'
import { SnackbarProvider } from 'notistack'
import { IconButton } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/CloseRounded'
import { useStyles } from './styles'

const FeedbackProvider: React.FunctionComponent<React.ReactNode> = ({
  children,
}): JSX.Element => {
  const classes = useStyles()

  const notistackRef: React.MutableRefObject<any> = React.useRef()

  const onClickDismiss = (key: string | number) => () => {
    notistackRef.current.closeSnackbar(key)
  }

  return (
    <SnackbarProvider
      maxSnack={3}
      autoHideDuration={5000}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      preventDuplicate
      classes={{
        root: classes.root,
        variantSuccess: classes.feedbackSuccess,
        variantError: classes.feedbackError,
        variantWarning: classes.feedbackWarning,
        variantInfo: classes.feedbackInfo,
      }}
      ref={notistackRef}
      action={key => (
        <IconButton
          aria-label="delete"
          classes={{ root: classes.buttonAction }}
          onClick={onClickDismiss(key)}
        >
          <CloseIcon />
        </IconButton>
      )}
    >
      {children}
    </SnackbarProvider>
  )
}

export default FeedbackProvider
