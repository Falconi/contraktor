import React from 'react'

import { InterfaceModal } from '../../prototypes'

import ClearIcon from '@material-ui/icons/ClearRounded'
import { IconButton } from '@material-ui/core'

import { useStyles } from './styles'

const Modal: React.FunctionComponent<InterfaceModal> = ({
  show = false,
  handleClose = () => {},
  children,
}): JSX.Element | null => {
  const [visibility, setVisibility] = React.useState<boolean>(false)
  const [display, setDisplay] = React.useState<boolean>(false)
  const classes = useStyles({ visibility })

  React.useEffect(() => {
    if (show) {
      setDisplay(show)
      // Wait DOM render
      setTimeout((): void => {
        setVisibility(show)
        const html = document.querySelector('html') as HTMLElement
        html.style.overflow = 'hidden'
      }, 10)
    } else {
      setVisibility(show)
      // Wait MODAL animation
      setTimeout((): void => {
        setDisplay(show)
        const html = document.querySelector('html') as HTMLElement
        html.style.overflow = 'inherit'
      }, 300)
    }
  }, [show])

  if (!display) {
    return null
  }

  return (
    <div className={classes.root}>
      <div className={classes.overlay} onClick={handleClose}></div>
      <div className={classes.container}>
        <IconButton
          className={classes.buttonClose}
          aria-label="close"
          onClick={handleClose}
        >
          <ClearIcon />
        </IconButton>
        <div className={classes.content}>{children}</div>
      </div>
    </div>
  )
}

export default Modal
