import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeModal = { visibility: boolean }
type TypeUseStyles = (
  props: TypeModal
) => Record<
  'root' | 'overlay' | 'container' | 'buttonClose' | 'content',
  string
>

export const useStyles: TypeUseStyles = makeStyles<Theme, TypeModal>(
  (theme: Theme) =>
    createStyles({
      root: {
        zIndex: 999999999,
        position: 'fixed',
        top: 0,
        left: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        padding: '15px',
        transition: 'all 0.3s ease',
        opacity: props => (props.visibility ? 1 : 0),
        visibility: props => (props.visibility ? 'visible' : 'hidden'),
      },
      overlay: {
        zIndex: 1,
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
      },
      container: {
        zIndex: 2,
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        maxWidth: '50rem',
        minHeight: '4rem',
        maxHeight: '80%',
        borderRadius: '8px',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #6A7180',
        overflow: 'auto',
      },
      buttonClose: {
        position: 'absolute',
        top: '0.5rem',
        right: '0.5rem',
        width: '3rem',
        height: '3rem',
        padding: '0.5rem',
        flexShrink: 0,
        color: theme.palette.text.primary,
        '& svg': {
          width: '2.5rem',
          height: '2.5rem',
          flexShrink: 0,
        },
      },
      content: {
        display: 'flex',
        flexDirection: 'column',
      },
    })
)
