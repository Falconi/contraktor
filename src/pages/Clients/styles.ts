import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeUseStyles = () => Record<
  'root' | 'pageHeader' | 'pageContent' | 'actions',
  string
>

export const useStyles: TypeUseStyles = makeStyles<Theme>((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      '& form': {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        margin: '-1.5rem',
      },
    },
    pageHeader: {
      display: 'flex',
      justifyContent: 'space-between',
      '& .pageTitle': {
        width: '100%',
        maxWidth: '50%',
      },
      '& .addButton': {
        marginTop: '0.5rem',
      },
    },
    pageContent: {
      display: 'flex',
      flexDirection: 'column',
      padding: '7rem 0 0 0',
    },
    actions: {
      display: 'flex',
      alignItems: 'center',
      '& .MuiButtonBase-root': {
        padding: '1rem',
      },
      '& .edit': {
        '& svg': {
          color: theme.palette.info.main,
        },
      },
      '& .delete': {
        '& svg': {
          color: theme.palette.error.main,
        },
      },
    },
    '@media(max-width: 767px)': {
      pageHeader: {
        flexDirection: 'column',
        '& .pageTitle': {
          maxWidth: '100%',
        },
        '& .addButton': {
          marginTop: '3rem',
        },
      },
      pageContent: {
        padding: '5rem 0 0 0',
      },
    },
  })
)
