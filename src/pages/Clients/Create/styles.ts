import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeUseStyles = () => Record<'root' | 'pageTitle' | 'pageContent', string>

export const useStyles: TypeUseStyles = makeStyles<Theme>(() =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      '& form': {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        margin: '-1.5rem',
      },
    },
    pageTitle: {
      width: '100%',
      maxWidth: '50%',
    },
    pageContent: {
      display: 'flex',
      flexDirection: 'column',
      padding: '7rem 0 0 0',
    },
    '@media(max-width: 767px)': {
      pageTitle: {
        maxWidth: '100%',
      },
      pageContent: {
        padding: '5rem 0 0 0',
      },
    },
  })
)
