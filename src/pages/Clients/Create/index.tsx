import React from 'react'

import {
  InterfaceClient,
  InterfaceFormClient,
  TypeFeedback,
} from '../../../prototypes'

import { useTheme, Theme, CircularProgress } from '@material-ui/core'
import { useStyles } from './styles'
import { useParams } from 'react-router-dom'
import { useSnackbar } from 'notistack'

import { Creators as ClientsActions } from '../../../store/ducks/clients'

import { useSelector, useDispatch } from 'react-redux'

import PageTitle from '../../../components/PageTitle'
import { FormInput, FormButton } from '../../../components/Form'

const formReset = (): InterfaceFormClient => {
  return {
    name: {
      require: true,
      value: '',
    },
    lastName: {
      require: true,
      value: '',
    },
    cpf: {
      require: true,
      value: '',
    },
    contactNumber: {
      require: true,
      value: '',
    },
    email: {
      require: true,
      value: '',
    },
  }
}

const ClientsCreate: React.FunctionComponent = (): JSX.Element => {
  const dispatch = useDispatch()
  const { enqueueSnackbar } = useSnackbar()

  const theme: Theme = useTheme()
  const classes = useStyles()

  const [listResetOnUnmount, setListResetOnUnmount] = React.useState<boolean>(
    false
  )

  const { id: contractId } = useParams()

  const [form, setForm] = React.useState<InterfaceFormClient>(formReset())
  const [showErrors, setShowErrors] = React.useState<boolean>(false)
  const [saveLoading, setSaveLoading] = React.useState<boolean>(false)
  const [pageLoading, setPageLoading] = React.useState<boolean>(true)

  const client: InterfaceClient | undefined = useSelector(
    (state: { Clients: { list: { data: Array<InterfaceClient> } } }) =>
      state.Clients.list.data &&
      state.Clients.list.data.find(
        (client: InterfaceClient) => client.id === contractId
      )
  )

  const onSave: {
    data?: {}
    error: boolean
    message: string
    method: string
  } = useSelector(
    (state: {
      Clients: {
        save: { data?: {}; error: boolean; message: string; method: string }
      }
    }) => state.Clients.save
  )

  React.useEffect(() => {
    if (!client && contractId !== 'novo') {
      dispatch(ClientsActions.clientById(contractId))
      setListResetOnUnmount(true)
      return
    }

    if (client) {
      const newForm: InterfaceFormClient = formReset()
      Object.keys(client).forEach((label: string) => {
        if (form[label]) {
          newForm[label].value = client[label]
        }
      })
      setForm(newForm)
    }

    setPageLoading(false)

    return () => {
      if (listResetOnUnmount) {
        dispatch(ClientsActions.clientsListReset())
      }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [client])

  React.useEffect((): void => {
    const feedback: { type?: TypeFeedback; message: string; method: string } = {
      type: onSave.error ? 'error' : 'success',
      message: onSave.message,
      method: onSave.method,
    }

    if (!feedback.message) {
      return
    }

    enqueueSnackbar(feedback.message, { variant: feedback.type })

    if (feedback.type === 'success' && feedback.method === 'create') {
      setShowErrors(false)
      setForm(formReset())
    }

    dispatch(ClientsActions.clientSaveReset())
    setSaveLoading(false)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [onSave])

  const save = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault()

    setShowErrors(true)
    let formValid = true

    Object.keys(form).forEach((input: any): void => {
      if (form[input].require && !form[input].value) {
        formValid = false
      }
    })

    if (!formValid) {
      return
    }

    setSaveLoading(true)

    const payload: InterfaceClient = {
      name: '',
      lastName: '',
      cpf: '',
      contactNumber: '',
      email: '',
    }

    Object.keys(form).forEach((label: string) => {
      payload[label] = form[label].value
    })

    let method: string = 'create'
    if (contractId !== 'novo') {
      method = 'update'
      payload.id = contractId
    }

    dispatch(ClientsActions.clientSave(payload, method))
  }

  const inputChange = (label: string, value: any): void => {
    setForm({
      ...form,
      [label]: {
        ...form[label],
        value,
      },
    })
  }

  return (
    <div className={classes.root}>
      <div className={classes.pageTitle}>
        <PageTitle
          title="Cadastro de clientes"
          description={`Preencha os campos abaixo para ${
            contractId === 'novo' ? 'adicionar um novo' : 'atualizar o'
          }  contrato.`}
          navigationLink="/clientes"
          navigationTitle="Voltar para a listagem"
        />
      </div>
      <div className={classes.pageContent}>
        {pageLoading && (
          <CircularProgress size={50} style={{ margin: '3rem auto 0 auto' }} />
        )}
        {!pageLoading && (
          <form onSubmit={save} noValidate autoComplete="off">
            <div className="column col-6">
              <FormInput
                label="Nome"
                value={form.name.value}
                helperText={
                  !form.name.value && form.name.require && showErrors
                    ? 'Campo Obrigatório'
                    : ''
                }
                error={!form.name.value && form.name.require && showErrors}
                onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
                  inputChange('name', e.currentTarget.value)
                }
                inputTextSize="1.6rem"
              />
            </div>
            <div className="column col-6">
              <FormInput
                label="Sobrenome"
                value={form.lastName.value}
                helperText={
                  !form.lastName.value && form.lastName.require && showErrors
                    ? 'Campo Obrigatório'
                    : ''
                }
                error={
                  !form.lastName.value && form.lastName.require && showErrors
                }
                onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
                  inputChange('lastName', e.currentTarget.value)
                }
                inputTextSize="1.6rem"
              />
            </div>
            <div className="column col-4">
              <FormInput
                label="Número de contato"
                value={form.contactNumber.value}
                helperText={
                  !form.contactNumber.value &&
                  form.contactNumber.require &&
                  showErrors
                    ? 'Campo Obrigatório'
                    : ''
                }
                error={
                  !form.contactNumber.value &&
                  form.contactNumber.require &&
                  showErrors
                }
                onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
                  inputChange('contactNumber', e.currentTarget.value)
                }
                inputTextSize="1.6rem"
              />
            </div>
            <div className="column col-4">
              <FormInput
                label="E-mail"
                value={form.email.value}
                helperText={
                  !form.email.value && form.email.require && showErrors
                    ? 'Campo Obrigatório'
                    : ''
                }
                error={!form.email.value && form.email.require && showErrors}
                onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
                  inputChange('email', e.currentTarget.value)
                }
                inputTextSize="1.6rem"
              />
            </div>
            <div className="column col-4">
              <FormInput
                label="Cpf"
                value={form.cpf.value}
                helperText={
                  !form.cpf.value && form.cpf.require && showErrors
                    ? 'Campo Obrigatório'
                    : ''
                }
                error={!form.cpf.value && form.cpf.require && showErrors}
                onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
                  inputChange('cpf', e.currentTarget.value)
                }
                inputTextSize="1.6rem"
              />
            </div>
            <div
              className="column col-4 col-xs-6"
              style={{ marginTop: '3rem' }}
            >
              <FormButton
                type="button"
                width="100%"
                height="5.5rem"
                fontSize="2rem"
                backgroundColor={theme.palette.secondary.main}
                onClick={(): void => {
                  setForm(formReset())
                  setShowErrors(false)
                }}
              >
                Limpar
              </FormButton>
            </div>
            <div
              className="column col-4 col-xs-6"
              style={{ marginTop: '3rem' }}
            >
              <FormButton
                type="submit"
                width="100%"
                height="5.5rem"
                fontSize="2rem"
                backgroundColor={theme.palette.primary.main}
                loading={saveLoading}
              >
                {contractId !== 'novo' ? 'Salvar' : 'Adicionar'}
              </FormButton>
            </div>
          </form>
        )}
      </div>
    </div>
  )
}

export default ClientsCreate
