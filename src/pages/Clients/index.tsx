import React from 'react'

import {
  InterfaceClient,
  InterfaceStoreClient,
  TypeFeedback,
} from '../../prototypes'

import { useHistory, useRouteMatch } from 'react-router-dom'
import {
  useTheme,
  Theme,
  IconButton,
  CircularProgress,
} from '@material-ui/core'
import { useStyles } from './styles'

import { Creators as ClientsActions } from '../../store/ducks/clients'

import { useSelector, useDispatch } from 'react-redux'
import { useSnackbar } from 'notistack'

import PageTitle from '../../components/PageTitle'
import { FormButton } from '../../components/Form'
import Table from '../../components/Table'
import ConfirmDialog from '../../components/ConfirmDialog'

import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'

const Clients: React.FunctionComponent = (): JSX.Element => {
  const dispatch = useDispatch()
  const { enqueueSnackbar } = useSnackbar()

  const router = useHistory()
  const match = useRouteMatch()

  const theme: Theme = useTheme()
  const classes = useStyles()

  const [tableLoading, setTableLoading] = React.useState<boolean>(false)
  const [confirmDialog, setConfirmDialog] = React.useState<{
    show: boolean
    client?: InterfaceClient
  }>({ show: false, client: {} })

  const listPagination: {
    currentPage: number
    limit: number
    count: number
  } = useSelector<
    {
      Clients: {
        listPagination: { currentPage: number; limit: number; count: number }
      }
    },
    any
  >(
    ({ Clients }): { currentPage: number; limit: number; count: number } =>
      Clients.listPagination
  )

  const clients: InterfaceStoreClient = useSelector<
    {
      Clients: { list: InterfaceStoreClient }
    },
    InterfaceStoreClient
  >(({ Clients }): InterfaceStoreClient => Clients.list)

  const onDelete: {
    error: boolean
    message: string
  } = useSelector(
    (state: {
      Clients: {
        delete: { error: boolean; message: string }
      }
    }) => state.Clients.delete
  )

  React.useEffect((): void => {
    if (!clients.data) {
      dispatch(
        ClientsActions.clientsList(
          listPagination.currentPage,
          listPagination.limit
        )
      )
      return
    }

    // Table container scroll to 0
    const element = document.querySelector(
      '.MuiTableContainer-root.makeStyles-container-20'
    ) as HTMLElement
    if (clients.data.length > 0 && element) {
      element.scrollTop = 0
    }

    setTableLoading(false)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [clients])

  React.useEffect((): void => {
    const feedback: { type?: TypeFeedback; message: string } = {
      type: onDelete.error ? 'error' : 'success',
      message: onDelete.message,
    }

    if (!feedback.message) {
      return
    }

    enqueueSnackbar(feedback.message, { variant: feedback.type })

    dispatch(ClientsActions.clientDeleteReset())
    setTableLoading(false)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [onDelete])

  const deleteItem = (client?: InterfaceClient) => {
    setTableLoading(true)
    setConfirmDialog({ ...confirmDialog, show: false })
    dispatch(ClientsActions.clientDelete(client))
  }

  const showConfirmDialog = (id?: number | string) => {
    setConfirmDialog({
      show: true,
      client:
        clients.data &&
        clients.data.find((client: InterfaceClient) => client.id === id),
    })
  }

  const changePage = (page: number): void => {
    setTableLoading(true)
    dispatch(ClientsActions.clientsList(page + 1, listPagination.limit))
  }

  const changePerPage = (perPage: number): void => {
    setTableLoading(true)
    dispatch(ClientsActions.clientsList(1, perPage))
  }

  return (
    <div className={classes.root}>
      <>
        <div className={classes.pageHeader}>
          <div className="pageTitle">
            <PageTitle
              title="Meus Clientes"
              description="Todos os seus clientes em uma única página? Sim, aqui é possível =)"
            />
          </div>
          <FormButton
            type="button"
            width="20rem"
            height="5.5rem"
            fontSize="2rem"
            backgroundColor={theme.palette.success.main}
            className="addButton"
            onClick={(): void => router.push(`${match.path}/novo`)}
          >
            + Novo Cliente
          </FormButton>
        </div>
        <div className={classes.pageContent}>
          {!clients.data && (
            <CircularProgress
              size={50}
              style={{ margin: '3rem auto 0 auto' }}
            />
          )}
          {clients.data && clients.data.length === 0 && (
            <p
              style={{
                marginTop: '5rem',
                fontSize: '1.8rem',
                color: theme.palette.secondary.main,
              }}
            >
              Você ainda não possuí clientes cadastrados
            </p>
          )}
          {clients.data && clients.data.length > 0 && (
            <Table
              onRowsPerPageChange={changePerPage}
              onPageChange={changePage}
              rowsPerPage={listPagination.limit}
              page={listPagination.currentPage - 1}
              count={listPagination.count}
              loading={tableLoading}
              columns={[
                {
                  id: 'actions',
                  label: '',
                  minWidth: '10rem',
                  align: 'left',
                },
                {
                  id: 'name',
                  label: 'Nome',
                  minWidth: '25rem',
                  align: 'left',
                },
                {
                  id: 'email',
                  label: 'E-mail',
                  minWidth: '25rem',
                  align: 'left',
                },
                {
                  id: 'cpf',
                  label: 'CPF',
                  minWidth: '18rem',
                  align: 'center',
                },
                {
                  id: 'contactNumber',
                  label: 'Contato',
                  minWidth: '18rem',
                  align: 'center',
                },
              ]}
              rows={clients.data.map((client: InterfaceClient) => ({
                actions: (
                  <div className={classes.actions}>
                    <IconButton
                      aria-label="edit"
                      className="edit"
                      onClick={(): void =>
                        router.push(`${match.path}/${client.id}`)
                      }
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                    <IconButton
                      aria-label="delete"
                      className="delete"
                      onClick={(): void => showConfirmDialog(client.id)}
                    >
                      <DeleteIcon fontSize="small" />
                    </IconButton>
                  </div>
                ),
                name: client.name + ' ' + client.lastName,
                email: client.email,
                cpf: client.cpf,
                contactNumber: client.contactNumber,
              }))}
            />
          )}
        </div>

        <ConfirmDialog
          show={confirmDialog.show}
          title={`Excluir`}
          description={`Tem certeza que deseja excluir o cliente ${
            confirmDialog.client?.name + ' ' + confirmDialog.client?.lastName
          } ? Após a exclusão, não será possível recuperar as informações.`}
          onConfirm={() => deleteItem(confirmDialog.client)}
          onCancel={() => setConfirmDialog({ ...confirmDialog, show: false })}
        />
      </>
    </div>
  )
}

export default Clients
