import React from 'react'

import { Link } from 'react-router-dom'

import {
  InterfaceContract,
  InterfaceStoreContract,
  InterfaceFormContract,
  InterfaceClient,
  TypeFeedback,
} from '../../../prototypes'

import { CloudUpload } from '../../../services/cloudinary'

import {
  useTheme,
  Theme,
  CircularProgress,
  LinearProgress,
} from '@material-ui/core'
import { useStyles } from './styles'
import { useParams } from 'react-router-dom'
import { useSnackbar } from 'notistack'

import { Creators as ContractsActions } from '../../../store/ducks/contracts'

import { useSelector, useDispatch } from 'react-redux'

import PageTitle from '../../../components/PageTitle'
import {
  FormInput,
  FormButton,
  FormInputDate,
  FormSelectMultiple,
} from '../../../components/Form'

const formReset = (): InterfaceFormContract => {
  return {
    title: {
      require: true,
      value: '',
    },
    file: {
      require: true,
      value: '',
    },
    initialDate: {
      require: true,
      value: new Date().toISOString(),
    },
    finalDate: {
      require: true,
      value: new Date().toISOString(),
    },
    clients: {
      require: true,
      value: [],
    },
  }
}

const ContractsCreate: React.FunctionComponent = (): JSX.Element => {
  const dispatch = useDispatch()
  const { enqueueSnackbar } = useSnackbar()

  const theme: Theme = useTheme()
  const classes = useStyles()

  const [listResetOnUnmount, setListResetOnUnmount] = React.useState<boolean>(
    false
  )

  const { id: contractId } = useParams()

  const [form, setForm] = React.useState<InterfaceFormContract>(formReset())
  const [selectClients, setSelectClients] = React.useState<
    Array<{ title: string | number; value: any }>
  >([])
  const [fileBase64, setFileBase64] = React.useState<
    string | ArrayBuffer | null
  >(null)
  const [showErrors, setShowErrors] = React.useState<boolean>(false)
  const [saveLoading, setSaveLoading] = React.useState<boolean>(false)
  const [pageLoading, setPageLoading] = React.useState<boolean>(true)

  const contract: InterfaceContract | undefined = useSelector(
    (state: { Contracts: { list: { data: Array<InterfaceContract> } } }) =>
      state.Contracts.list.data &&
      state.Contracts.list.data.find(
        (contract: InterfaceContract) => contract.id === contractId
      )
  )

  const clientsOptions: InterfaceStoreContract = useSelector<
    {
      Contracts: { clientsOptions: InterfaceStoreContract }
    },
    InterfaceStoreContract
  >(({ Contracts }): InterfaceStoreContract => Contracts.clientsOptions)

  const onSave: {
    data?: {}
    error: boolean
    message: string
    method: string
  } = useSelector(
    (state: {
      Contracts: {
        save: { data?: {}; error: boolean; message: string; method: string }
      }
    }) => state.Contracts.save
  )

  React.useEffect((): any => {
    if (!contract && contractId !== 'novo') {
      dispatch(ContractsActions.contractById(contractId))
      setListResetOnUnmount(true)
      return
    }

    if (!clientsOptions.data) {
      dispatch(ContractsActions.clientsOptions())
    }

    if (contract) {
      const newForm: InterfaceFormContract = formReset()
      setFileBase64(null)
      Object.keys(contract).forEach((label: string) => {
        if (form[label]) {
          newForm[label].value = contract[label]
        }
        if (label === 'clients') {
          setSelectClients(
            newForm.clients.value.map((client: InterfaceClient) => ({
              title:
                'Nome: ' +
                client.name +
                ' ' +
                client.lastName +
                ' - CPF: ' +
                client.cpf,
              value: client,
            }))
          )
        }
      })
      setForm(newForm)
    }

    setPageLoading(false)

    return () => {
      if (listResetOnUnmount) {
        dispatch(ContractsActions.contractsListReset())
      }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [contract])

  React.useEffect((): void => {
    const feedback: { type?: TypeFeedback; message: string; method: string } = {
      type: onSave.error ? 'error' : 'success',
      message: onSave.message,
      method: onSave.method,
    }

    if (!feedback.message) {
      return
    }

    enqueueSnackbar(feedback.message, { variant: feedback.type })

    if (feedback.type === 'success' && feedback.method === 'create') {
      setShowErrors(false)
      setForm(formReset())
      setSelectClients([])
    }

    dispatch(ContractsActions.contractSaveReset())
    setSaveLoading(false)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [onSave])

  const save = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    setShowErrors(true)
    let formValid = true
    Object.keys(form).forEach((input: any): void => {
      if (form[input].require && !form[input].value) {
        formValid = false
      }
    })

    if (selectClients.length === 0) {
      formValid = false
    }

    if (!formValid) {
      return
    }

    setSaveLoading(true)

    const payload: InterfaceContract = {
      title: '',
      file: '',
      initialDate: '',
      finalDate: '',
      clients: [],
    }

    Object.keys(form).forEach((label: string) => {
      payload[label] = form[label].value
    })

    if (fileBase64) {
      const upload = await CloudUpload(fileBase64)
      payload.file = upload.secure_url
    }

    let method: string = 'create'
    if (contractId !== 'novo') {
      method = 'update'
      payload.id = contractId
    }

    dispatch(ContractsActions.contractSave(payload, method))
  }

  const inputChange = (label: string, value: any): void => {
    setForm({
      ...form,
      [label]: {
        ...form[label],
        value,
      },
    })
  }

  const handleSelect = (
    options: Array<{ title: string | number; value: Array<InterfaceClient> }>
  ) => {
    setSelectClients(options)
    setForm({
      ...form,
      clients: {
        ...form.clients,
        value: options.map(
          (option: { title: string | number; value: any }) => option.value
        ),
      },
    })
  }

  const getSelectClientsOptions = (): Array<{
    title: string | number
    value: any
  }> => {
    const options: Array<{ title: string | number; value: any }> = []

    if (clientsOptions.data && clientsOptions.data.length > 0) {
      clientsOptions.data.forEach((client: InterfaceClient) => {
        if (
          !selectClients.find(
            (item: { title: string | number; value: InterfaceClient }) =>
              client.id === item.value.id
          )
        ) {
          options.push({
            title:
              'Nome: ' +
              client.name +
              ' ' +
              client.lastName +
              ' - CPF: ' +
              client.cpf,
            value: client,
          })
        }
      })
    }

    return options
  }

  const handleFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.target.files.length > 0) {
      const file: File = e.target.files[0]
      const reader: FileReader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = (): void => {
        setForm({ ...form, file: { ...form.file, value: file.name } })
        setFileBase64(reader.result)
      }
    }
  }

  return (
    <div className={classes.root}>
      <div className={classes.pageTitle}>
        <PageTitle
          title="Cadastro de contrato"
          description={`Preencha os campos abaixo para ${
            contractId === 'novo' ? 'adicionar um novo' : 'atualizar o'
          }  contrato.`}
          navigationLink="/contratos"
          navigationTitle="Voltar para a listagem"
        />
      </div>
      <div className={classes.pageContent}>
        {pageLoading && (
          <CircularProgress size={50} style={{ margin: '3rem auto 0 auto' }} />
        )}
        {!pageLoading && (
          <form onSubmit={save} noValidate autoComplete="off">
            <div className="column">
              <FormInput
                label="Título"
                value={form.title.value}
                helperText={
                  !form.title.value && form.title.require && showErrors
                    ? 'Campo Obrigatório'
                    : ''
                }
                error={!form.title.value && form.title.require && showErrors}
                onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
                  inputChange('title', e.currentTarget.value)
                }
                inputTextSize="1.6rem"
              />
            </div>
            <div className="column col-4">
              <p style={{ marginBottom: '0.5rem' }}>
                Documento (PDF, DOC, DOCX)
              </p>
              <div className={classes.boxFile}>
                <input
                  accept=".doc,.docx,.pdf"
                  style={{ display: 'none' }}
                  id="contained-button-file"
                  type="file"
                  onChange={handleFile}
                />
                <label
                  htmlFor="contained-button-file"
                  style={{ cursor: 'pointer' }}
                >
                  <FormButton
                    type="button"
                    width="12rem"
                    height="4rem"
                    fontSize="1.6rem"
                    backgroundColor={theme.palette.secondary.main}
                    component="span"
                  >
                    {form.file.value ? 'Alterar' : 'Anexar'}
                  </FormButton>
                  <p
                    style={{
                      wordBreak: 'break-all',
                      fontSize: '1.2rem',
                      color:
                        !form.file.value && form.file.require && showErrors
                          ? theme.palette.error.main
                          : theme.palette.text.secondary,
                    }}
                  >
                    {(form.file.value &&
                      form.file.value
                        .split('.')
                        .slice(0, form.file.value.split('.').length - 1)
                        .join('.') + '.jpg') ||
                      'Nenhum arquivo selecionado'}
                  </p>
                </label>
              </div>
            </div>
            <div className="column col-4">
              <p style={{ marginBottom: '0.5rem' }}>Data de início</p>
              <FormInputDate
                width={'100%'}
                value={form.initialDate.value}
                onChange={(value: any): void =>
                  inputChange('initialDate', value)
                }
              />
            </div>
            <div className="column col-4">
              <p style={{ marginBottom: '0.5rem' }}>Data de vencimento</p>
              <FormInputDate
                width={'100%'}
                value={form.finalDate.value}
                onChange={(value: any): void => inputChange('finalDate', value)}
              />
            </div>
            <div className="column">
              <h2>
                Escolha abaixo os clientes que serão vinculados a este contrato
              </h2>
              {!clientsOptions.data && (
                <LinearProgress style={{ marginTop: '1rem' }} />
              )}
              {clientsOptions.data && clientsOptions.data.length === 0 && (
                <>
                  <hr />
                  <p style={{ color: theme.palette.text.secondary }}>
                    Você ainda não possuí clientes cadastrados,{' '}
                    <Link
                      to="/clientes/novo"
                      style={{
                        color: theme.palette.primary.main,
                        textDecoration: 'underline',
                      }}
                    >
                      clique aqui para cadastrar...
                    </Link>
                  </p>
                </>
              )}
              {clientsOptions.data && clientsOptions.data.length > 0 && (
                <>
                  <hr />
                  <FormSelectMultiple
                    options={getSelectClientsOptions().map(item => item)}
                    onChange={handleSelect}
                    label="Clique para selecionar"
                    placeholder="Filtrar por nome ou cpf"
                    value={selectClients}
                  />
                </>
              )}
              {form.clients.require &&
                selectClients.length === 0 &&
                showErrors && (
                  <p
                    style={{
                      color: theme.palette.error.main,
                      fontSize: '1.4rem',
                      fontWeight: 400,
                    }}
                  >
                    Campo Obrigatório
                  </p>
                )}
            </div>
            <div
              className="column col-4 col-xs-6"
              style={{ marginTop: '3rem' }}
            >
              <FormButton
                type="button"
                width="100%"
                height="5.5rem"
                fontSize="2rem"
                backgroundColor={theme.palette.secondary.main}
                onClick={(): void => {
                  setForm(formReset())
                  setSelectClients([])
                  setFileBase64(null)
                  setShowErrors(false)
                }}
              >
                Limpar
              </FormButton>
            </div>
            <div
              className="column col-4 col-xs-6"
              style={{ marginTop: '3rem' }}
            >
              <FormButton
                type="submit"
                width="100%"
                height="5.5rem"
                fontSize="2rem"
                backgroundColor={theme.palette.primary.main}
                loading={saveLoading}
              >
                {contractId !== 'novo' ? 'Salvar' : 'Adicionar'}
              </FormButton>
            </div>
          </form>
        )}
      </div>
    </div>
  )
}

export default ContractsCreate
