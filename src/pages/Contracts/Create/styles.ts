import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeUseStyles = () => Record<
  'root' | 'pageTitle' | 'pageContent' | 'boxFile',
  string
>

export const useStyles: TypeUseStyles = makeStyles<Theme>((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      '& form': {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        margin: '-1.5rem',
      },
      '& h2': {
        marginTop: '4rem',
        fontSize: '2rem',
        fontWeight: 400,
        color: theme.palette.text.secondary,
      },
      '& hr': {
        marginBottom: '3rem',
        borderColor: theme.palette.text.secondary,
      },
      '& .MuiAutocomplete-root': {
        marginBottom: '1rem',
      },
    },
    pageTitle: {
      width: '100%',
      maxWidth: '50%',
    },
    pageContent: {
      display: 'flex',
      flexDirection: 'column',
      padding: '7rem 0 0 0',
    },
    boxFile: {
      '& label': {
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        '& p': {
          marginLeft: '1rem',
        },
      },
    },
    '@media(max-width: 767px)': {
      pageTitle: {
        maxWidth: '100%',
      },
      pageContent: {
        padding: '5rem 0 0 0',
      },
    },
  })
)
