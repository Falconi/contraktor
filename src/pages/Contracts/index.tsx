import React from 'react'

import Moment from 'moment'

import {
  InterfaceClient,
  InterfaceContract,
  InterfaceStoreContract,
  TypeFeedback,
} from '../../prototypes'

import { useHistory, useRouteMatch } from 'react-router-dom'
import {
  useTheme,
  Theme,
  IconButton,
  CircularProgress,
} from '@material-ui/core'
import { useStyles } from './styles'

import { Creators as ContractsActions } from '../../store/ducks/contracts'

import { useSelector, useDispatch, DefaultRootState } from 'react-redux'
import { useSnackbar } from 'notistack'

import PageTitle from '../../components/PageTitle'
import { FormButton } from '../../components/Form'
import Table from '../../components/Table'
import Modal from '../../components/Modal'
import ClientList from '../../components/ClientList'
import ConfirmDialog from '../../components/ConfirmDialog'

import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import PeopleIcon from '@material-ui/icons/People'

const Contracts: React.FunctionComponent = (): JSX.Element => {
  const dispatch = useDispatch()
  const { enqueueSnackbar } = useSnackbar()

  const router = useHistory()
  const match = useRouteMatch()

  const theme: Theme = useTheme()
  const classes = useStyles()

  const [tableLoading, setTableLoading] = React.useState<boolean>(false)
  const [confirmDialog, setConfirmDialog] = React.useState<{
    show: boolean
    contract?: InterfaceContract
  }>({ show: false, contract: {} })

  const [modalClients, setModalClients] = React.useState<{
    show: boolean
    clients?: Array<InterfaceClient>
  }>({ show: false, clients: [] })

  const listPagination: {
    currentPage: number
    limit: number
    count: number
  } = useSelector<
    {
      Contracts: {
        listPagination: { currentPage: number; limit: number; count: number }
      }
    },
    any
  >(
    ({ Contracts }): { currentPage: number; limit: number; count: number } =>
      Contracts.listPagination
  )

  const contracts: InterfaceStoreContract = useSelector<
    {
      Contracts: { list: InterfaceStoreContract }
    },
    DefaultRootState
  >(({ Contracts }): InterfaceStoreContract => Contracts.list)

  const onDelete: {
    error: boolean
    message: string
  } = useSelector(
    (state: {
      Contracts: {
        delete: { error: boolean; message: string }
      }
    }) => state.Contracts.delete
  )

  React.useEffect((): void => {
    if (!contracts.data) {
      dispatch(
        ContractsActions.contractsList(
          listPagination.currentPage,
          listPagination.limit
        )
      )
      return
    }

    // Table container scroll to 0
    const element = document.querySelector(
      '.MuiTableContainer-root.makeStyles-container-20'
    ) as HTMLElement
    if (contracts.data.length > 0 && element) {
      element.scrollTop = 0
    }

    setTableLoading(false)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [contracts])

  React.useEffect((): void => {
    const feedback: { type?: TypeFeedback; message: string } = {
      type: onDelete.error ? 'error' : 'success',
      message: onDelete.message,
    }

    if (!feedback.message) {
      return
    }

    enqueueSnackbar(feedback.message, { variant: feedback.type })

    dispatch(ContractsActions.contractDeleteReset())
    setTableLoading(false)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [onDelete])

  const deleteItem = (contract?: InterfaceContract) => {
    setTableLoading(true)
    setConfirmDialog({ ...confirmDialog, show: false })
    dispatch(ContractsActions.contractDelete(contract))
  }

  const showConfirmDialog = (id?: number | string) => {
    setConfirmDialog({
      show: true,
      contract:
        contracts.data &&
        contracts.data.find(
          (contract: InterfaceContract) => contract.id === id
        ),
    })
  }

  const changePage = (page: number): void => {
    setTableLoading(true)
    dispatch(ContractsActions.contractsList(page + 1, listPagination.limit))
  }

  const changePerPage = (perPage: number): void => {
    setTableLoading(true)
    dispatch(ContractsActions.contractsList(1, perPage))
  }

  return (
    <div className={classes.root}>
      <div className={classes.pageHeader}>
        <div className="pageTitle">
          <PageTitle
            title="Meus Contratos"
            description="Chega de papéis, gerencie seus contratos com apenas alguns cliques."
          />
        </div>
        <FormButton
          type="button"
          width="20rem"
          height="5.5rem"
          fontSize="2rem"
          backgroundColor={theme.palette.success.main}
          className="addButton"
          onClick={(): void => router.push(`${match.path}/novo`)}
        >
          + Novo Contrato
        </FormButton>
      </div>
      <div className={classes.pageContent}>
        {!contracts.data && (
          <CircularProgress size={50} style={{ margin: '3rem auto 0 auto' }} />
        )}
        {contracts.data && contracts.data.length === 0 && (
          <p
            style={{
              marginTop: '5rem',
              fontSize: '1.8rem',
              color: theme.palette.secondary.main,
            }}
          >
            Você ainda não possuí contratos cadastrados
          </p>
        )}
        {contracts.data && contracts.data.length > 0 && (
          <Table
            onRowsPerPageChange={changePerPage}
            onPageChange={changePage}
            rowsPerPage={listPagination.limit}
            page={listPagination.currentPage - 1}
            count={listPagination.count}
            loading={tableLoading}
            columns={[
              {
                id: 'actions',
                label: '',
                minWidth: '15rem',
                align: 'left',
              },
              {
                id: 'code',
                label: 'Código',
                minWidth: '10rem',
                align: 'left',
              },
              {
                id: 'title',
                label: 'Título',
                minWidth: '30rem',
                align: 'left',
              },
              {
                id: 'initialDate',
                label: 'Data inicial',
                minWidth: '15rem',
                align: 'center',
              },
              {
                id: 'finalDate',
                label: 'Data vencimento',
                minWidth: '18rem',
                align: 'center',
              },
              {
                id: 'file',
                label: 'Download',
                minWidth: '15rem',
                align: 'center',
              },
            ]}
            rows={contracts.data.map((contract: InterfaceContract) => ({
              actions: (
                <div className={classes.actions}>
                  <IconButton
                    aria-label="details"
                    className="details"
                    onClick={(): void =>
                      setModalClients({ show: true, clients: contract.clients })
                    }
                  >
                    <PeopleIcon fontSize="small" />
                  </IconButton>
                  <IconButton
                    aria-label="edit"
                    className="edit"
                    onClick={(): void =>
                      router.push(`${match.path}/${contract.id}`)
                    }
                  >
                    <EditIcon fontSize="small" />
                  </IconButton>
                  <IconButton
                    aria-label="delete"
                    className="delete"
                    onClick={(): void => showConfirmDialog(contract.id)}
                  >
                    <DeleteIcon fontSize="small" />
                  </IconButton>
                </div>
              ),
              code: contract.code,
              title: contract.title,
              initialDate:
                contract.initialDate &&
                Moment(contract.initialDate).format('DD/MM/YYYY'),
              finalDate:
                contract.finalDate &&
                Moment(contract.finalDate).format('DD/MM/YYYY'),
              file: (
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href={
                    contract.file &&
                    contract.file
                      .split('.')
                      .slice(0, contract.file.split('.').length - 1)
                      .join('.') + '.jpg'
                  }
                  style={{
                    color: theme.palette.primary.main,
                    textDecoration: 'underline',
                  }}
                >
                  Download
                </a>
              ),
            }))}
          />
        )}
      </div>

      <Modal
        show={modalClients.show}
        handleClose={() => setModalClients({ ...modalClients, show: false })}
      >
        <ClientList list={modalClients.clients} />
      </Modal>

      <ConfirmDialog
        show={confirmDialog.show}
        title={`Excluir`}
        description={`Tem certeza que deseja excluir o contrato ${confirmDialog.contract?.title} ? Após a exclusão, não será possível recuperar as informações.`}
        onConfirm={() => deleteItem(confirmDialog.contract)}
        onCancel={() => setConfirmDialog({ ...confirmDialog, show: false })}
      />
    </div>
  )
}

export default Contracts
