import { makeStyles, Theme, createStyles } from '@material-ui/core'

type TypeUseStyles = () => Record<'root' | 'pageTitle' | 'pageContent', string>

export const useStyles: TypeUseStyles = makeStyles<Theme>((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      '& .colorSecondary': {
        color: theme.palette.text.secondary,
      },
      '& ul': {
        display: 'flex',
        flexWrap: 'wrap',
        marginTop: '1.5rem',
        '& li': {
          margin: '1rem 5rem 1rem 0',
          '& p': {
            fontSize: '2rem',
          },
          '&:last-child': {
            marginRight: 0,
          },
        },
      },
      '& .clientList': {
        marginTop: '5rem',
        '& .clientListHeader': {
          padding: '0 0 1rem 0',
          borderBottom: `1px solid ${theme.palette.text.secondary}`,
          '& span': {
            color: theme.palette.info.main,
          },
        },
        '& .clientListBody': {
          padding: '3rem 0 0 0',
        },
        '& .clientListBox': {
          padding: 0,
          border: 0,
        },
      },
    },
    pageTitle: {
      width: '100%',
      maxWidth: '50%',
    },
    pageContent: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: '7rem 0 0 0',
      '& .column': {
        padding: 0,
      },
    },
    '@media(max-width: 767px)': {
      pageTitle: {
        maxWidth: '100%',
      },
    },
  })
)
