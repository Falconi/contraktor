import React from 'react'

import Moment from 'moment'

import { InterfaceContract } from '../../prototypes'

import { useTheme, Theme } from '@material-ui/core'
import { useSelector, useDispatch } from 'react-redux'
import { useStyles } from './styles'

import { Creators as ContractsActions } from '../../store/ducks/contracts'

import PageTitle from '../../components/PageTitle'
import { FormSearch } from '../../components/Form'
import ClientList from '../../components/ClientList'

const Home: React.FunctionComponent = (): JSX.Element => {
  const dispatch = useDispatch()
  const classes = useStyles()
  const theme: Theme = useTheme()

  const [search, setSearch] = React.useState<string | number>('')
  const [loading, setLoading] = React.useState<boolean>(false)

  const contract: InterfaceContract = useSelector(
    (state: { Contracts: { details: { data: InterfaceContract } } }) =>
      state.Contracts.details.data
  )

  React.useEffect((): any => {
    if (contract) {
      setLoading(false)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [contract])

  React.useEffect((): any => {
    return () => {
      dispatch(ContractsActions.contractByCodeReset())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const handleDoSearch = (value: string | number): void => {
    setLoading(true)
    dispatch(ContractsActions.contractByCode(value))
  }

  const handleResetSearch = (): void => {
    setSearch('')
    dispatch(ContractsActions.contractByCodeReset())
  }

  return (
    <div className={classes.root}>
      <div className={classes.pageTitle}>
        <PageTitle
          title="Consulta rápida"
          description="Para fazer uma consulta, informe o código do contrato no campo abaixo."
        />
      </div>
      <div className={classes.pageContent}>
        <div className="column col-6">
          <FormSearch
            placeholder="Buscar"
            search={search}
            setSearch={setSearch}
            loading={loading}
            disabled={loading}
            handleDoSearch={handleDoSearch}
            handleResetSearch={handleResetSearch}
            width="100%"
            height="5.5rem"
            fontSize="2rem"
          />
        </div>
        {contract && Object.keys(contract).length === 0 && (
          <div className="column" style={{ marginTop: '10rem' }}>
            <h3
              style={{
                color: theme.palette.text.secondary,
                textAlign: 'center',
              }}
            >
              Contrato não encontrado, verifique se o código informando está
              correto.
            </h3>
          </div>
        )}

        {contract && Object.keys(contract).length > 0 && (
          <div className="column" style={{ marginTop: '10rem' }}>
            <h2 style={{ color: theme.palette.success.main }}>
              {contract.code} - {contract.title}
            </h2>
            <hr />
            <ul>
              <li>
                <p>
                  Data inicial:{' '}
                  <span className="colorSecondary">
                    {Moment(contract.initialDate).format('DD/MM/YYYY')}
                  </span>
                </p>
              </li>
              <li>
                <p>
                  Data do vencimento:{' '}
                  <span className="colorSecondary">
                    {Moment(contract.finalDate).format('DD/MM/YYYY')}
                  </span>
                </p>
              </li>
              <li>
                <p>
                  Arquivo:{' '}
                  <span className="colorSecondary">
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href={
                        contract.file &&
                        contract.file
                          .split('.')
                          .slice(0, contract.file.split('.').length - 1)
                          .join('.') + '.jpg'
                      }
                      style={{
                        color: theme.palette.primary.main,
                        textDecoration: 'underline',
                      }}
                    >
                      Download
                    </a>
                  </span>
                </p>
              </li>
            </ul>
            <ClientList list={contract.clients} />
          </div>
        )}
      </div>
    </div>
  )
}

export default Home
