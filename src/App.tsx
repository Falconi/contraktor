import React from 'react'
import { Provider } from 'react-redux'
import FeedbackProvider from './components/FeedbackProvider'

import CssBaseLine from '@material-ui/core/CssBaseline'
import { ThemeProvider } from '@material-ui/core'
import { ContraktorTheme } from './styles/theme'
import { GlobalStyle } from './styles/global'

import MainRoutes from './routes'

import Store from './store'

const App: React.FunctionComponent = () => {
  return (
    <ThemeProvider theme={ContraktorTheme}>
      <CssBaseLine />
      <GlobalStyle />
      <FeedbackProvider>
        <Provider store={Store}>
          <MainRoutes />
        </Provider>
      </FeedbackProvider>
    </ThemeProvider>
  )
}

export default App
